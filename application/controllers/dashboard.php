<?php
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->tank_auth->is_logged_in() && $this->session->userdata('tipo_usuario') != 1){
            $this->session->sess_destroy();
            delete_cookie("ci_session");
            redirect(base_url());
        }
        $this->lang->load('tank_auth');
    }

    /**
     * El index es la pagina principal del sistema, donde el cliente va a poder ver sus datos economicos por mes actual, mes en especifico o por un rango de mes a mes
     * Aqui se va a desplegar, los ingresos, los gastos (ambos sin iva), las cuentas por pagar, las cuentas por cobrar, el porcentaje del mes en relacion a ingresos - gastos
     * Las utilidades que se llevan por año, y el iva que se tiene que pagar.
     **/
	public function index() {
        $mes = date("F");
        $year = date("Y");

//        Se llama a la funcion "calculoMovimientos" del modelo "Factura model" y se le pasa el id del usuario logueado como variable de entrada
//        Esta funcion devuelve el subtotal, que tipo de comprobante y si ya esta pagado, para poder hacer el calculo de las utilidadees del cliente
        $datos = $this->factura_model->calculoMovimientos($this->tank_auth->get_user_id(), $mes, $year);

//        Se declaran las variables que seran el calculo total de los dos valores necesarios para poder sacar las utilidades
        $total_ingresos = 0;
        $total_egreso = 0;

//        Este ciclo lo que hace es:
//          1) Revisa que tipo de comprobante es
//          2) En caso de ser "ingreso, se suma a la variable donde va a estar el total de los ingresos
//          3) En caso de ser gasto, se suma a la variable donde va a estar el total de los gastos
//        Si no se cumple ninguno de los dos casos, se dejan las variables en 0
        foreach ($datos as $row) {
            if($row->tipo_comprobante == "ingreso") {
                $total_ingresos += $row->subtotal;
            }
            elseif($row->tipo_comprobante == "gasto"){
                $total_egreso += $row->subtotal;
            }
            else {
                $total_ingresos = 0;
                $total_egreso = 0;
            }
        }

//        Se toma el subtotal, mes y tipo de comprobante del cliente, para poder hacer el calculo de las utilidades del cliente, por año
        $impuestos = $this->factura_model->tomarMeses($this->tank_auth->get_user_id());

//        Se declara el arreglo que va a tener el calculo de utilidades del cliente por mes
        $impuestos_mes = array(
            'enero' => 0.0,
            'febrero' => 0.0,
            'marzo' => 0.0,
            'abril' => 0.0,
            'mayo' => 0.0,
            'junio' => 0.0,
            'julio' => 0.0,
            'agosto' => 0.0,
            'septiembre' => 0.0,
            'octubre' => 0.0,
            'noviembre' => 0.0,
            'diciembre' => 0.0,
        );


//        Se declara el arreglo que va a tener el total de los ingreso del cliente por mes
        $ingresos_mes = array(
            0 => 0.0,
            1 => 0.0,
            2 => 0.0,
            3 => 0.0,
            4 => 0.0,
            5 => 0.0,
            6 => 0.0,
            7 => 0.0,
            8 => 0.0,
            9 => 0.0,
            10 => 0.0,
            11 => 0.0,
        );

//        Se declara el arreglo que va a tener el total de los egreso del cliente por mes
        $egresos_mes = array(
            0 => 0.0,
            1 => 0.0,
            2 => 0.0,
            3 => 0.0,
            4 => 0.0,
            5 => 0.0,
            6 => 0.0,
            7 => 0.0,
            8 => 0.0,
            9 => 0.0,
            10 => 0.0,
            11 => 0.0,
        );

//        Este ciclo lo que hace es:
//          1) Se ve por fila del arreglo, que tipo de mes es
//          2) Una ves que se determina el mes, se revisa que tipo de comprobante es
//          3) En caso de ser ingreso, se suma al arreglo en la posicion que le corresponde
//          4) En case de ser gasto, se resta al arreglo en la posicion que le corresponde
        foreach($impuestos as $row ){
            switch($row->mes_pago){
                case "January":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['enero'] += $row->subtotal;
                        $ingresos_mes[0] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['enero'] -= $row->subtotal;
                        $egresos_mes[0] -= $row->subtotal;
                    }
                    break;
                case "February":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['febrero'] += $row->subtotal;
                        $ingresos_mes[1] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['febrero'] -= $row->subtotal;
                        $egresos_mes[1] -= $row->subtotal;
                    }
                    break;
                case "March":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['marzo'] += $row->subtotal;
                        $ingresos_mes[2] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['marzo'] -= $row->subtotal;
                        $egresos_mes[2] -= $row->subtotal;
                    }
                    break;
                case "April":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['abril'] += $row->subtotal;
                        $ingresos_mes[3] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['abril'] -= $row->subtotal;
                        $egresos_mes[3] -= $row->subtotal;
                    }
                    break;
                case "May":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['mayo'] += $row->subtotal;
                        $ingresos_mes[4] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['mayo'] -= $row->subtotal;
                        $egresos_mes[4] -= $row->subtotal;
                    }
                    break;
                case "June":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['junio'] += $row->subtotal;
                        $ingresos_mes[5] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['junio'] -= $row->subtotal;
                        $egresos_mes[5] -= $row->subtotal;
                    }
                    break;
                case "July":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['julio'] += $row->subtotal;
                        $ingresos_mes[6] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['julio'] -= $row->subtotal;
                        $egresos_mes[6] -= $row->subtotal;
                    }
                    break;
                case "August":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['agosto'] += $row->subtotal;
                        $ingresos_mes[7] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['agosto'] -= $row->subtotal;
                        $egresos_mes[7] -= $row->subtotal;
                    }
                    break;
                case "September":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['septiembre'] += $row->subtotal;
                        $ingresos_mes[8] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['septiembre'] -= $row->subtotal;
                        $egresos_mes[8] -= $row->subtotal;
                    }
                    break;
                case "October":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['octubre'] += $row->subtotal;
                        $ingresos_mes[9] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['octubre'] -= $row->subtotal;
                        $egresos_mes[9] -= $row->subtotal;
                    }
                    break;
                case "November":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['noviembre'] += $row->subtotal;
                        $ingresos_mes[10] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['noviembre'] -= $row->subtotal;
                        $egresos_mes[10] -= $row->subtotal;
                    }
                    break;
                case "December":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['diciembre'] += $row->subtotal;
                        $ingresos_mes[11] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['diciembre'] -= $row->subtotal;
                        $egresos_mes[11] -= $row->subtotal;
                    }
                    break;
            }
        }

//        Se declara la variable que va a tener el total de la utilidad del cliente por mes
        $resultado_utilidades = array();

//        Este ciclo lo que hace es:
//          1) Se por fila del arreglo, que tipo de mes es
//          2) Una ves que se determina el mes, se revisa que tipo de comprobante es
//          3) En caso de ser ingreso, se suma al arreglo en la posicion que le corresponde
//          4) En case de ser gasto, se resta al arreglo en la posicion que le corresponde
        foreach($impuestos_mes as $mes) {
            $resultado_utilidades[] = $mes;
        }

        $js_array = json_encode($resultado_utilidades);

        $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

        $iva = $this->factura_model->calcularIvaPorPagar();
        $iva_a_pagar = 0.0;
        if(!empty($iva)) {
            foreach($iva as $info) {
                if($info->tipo_comprobante == 'ingreso') {
                    $iva_a_pagar += $info->iva;
                }
                else {
                    $iva_a_pagar -= $info->iva;
                }
            }

        }

        $mes_actual = $this->fecha->getMesActual(date("F"));

        $data = array(
            'usuario' => $nombreUsuario->nombre_completo,
            'fecha' => date("j")." ".$mes_actual." ".date("Y"),
            'por_cobrar' => $this->getCuentasCobrar(),
            'por_pagar' => $this->getCuentasPagar(),
            'ingresos' => $total_ingresos,
            'egresos' => $total_egreso,
            'utilidad_mes' => $total_ingresos - $total_egreso,
            'utilidades_year' => $js_array,
            'ingresos_mes' => $ingresos_mes,
            'egresos_mes' => $egresos_mes,
            'iva_por_pagar' => $iva_a_pagar,
        );

		$this->load->view('front/home_view', $data);
	}

    public function revisarContador() {
        $contadores = $this->contadores_model->verificarContador($this->tank_auth->get_user_id());
        echo(json_encode($contadores));
    }

    public function aceptarContador() {

        $contador = base64_decode($this->encrypt->decode($this->input->post("contador")));

        $this->form_validation->set_rules('contador', 'Contador', 'trim|xss_clean');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');

        if ($this->form_validation->run()) {
            $setContador = $this->contadores_model->setContador($this->tank_auth->get_user_id(), $contador);

            if($setContador ) {
                $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

                $notificacion = $this->contadores_model->notificarContador($this->tank_auth->get_user_id(), $contador, "El usuario <b>".$nombreUsuario->nombre_completo."</b> lo ha agregado como contador.");

                if($notificacion) {
                    $datosContador = $this->contadores_model->getInfoContador($contador);

                    $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

                    $mes_actual = $this->fecha->getMesActual(date("F"));

                    $data = array(
                        'usuario' => $nombreUsuario->nombre_completo,
                        'fecha' => date("j")." ".$mes_actual." ".date("Y"),
                        'nombre_contador' => $datosContador->nombre_completo,
                        'contador' => base64_encode($datosContador->id_user),
                        'mensaje' => '<div class="alert alert-success" role="alert">Se ha agregado su contador con exito!</div>',
                    );

                    $this->load->view('front/info_contador_view',$data);
                }
                else {
                    $datosContador = $this->contadores_model->getInfoContador($contador);

                    $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

                    $mes_actual = $this->fecha->getMesActual(date("F"));

                    $data = array(
                        'usuario' => $nombreUsuario->nombre_completo,
                        'fecha' => date("j")." ".$mes_actual." ".date("Y"),
                        'nombre_contador' => $datosContador->nombre_completo,
                        'contador' => base64_encode($datosContador->id_user),
                        'mensaje' => '<div class="alert alert-warning" role="alert">Hubo un error al asignar al contador, por favor, contacte al administrador.</div>',
                    );

                    $this->load->view('front/info_contador_view',$data);
                }
            }
            else {
                $datosContador = $this->contadores_model->getInfoContador($contador);

                $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

                $mes_actual = $this->fecha->getMesActual(date("F"));

                $data = array(
                    'usuario' => $nombreUsuario->nombre_completo,
                    'fecha' => date("j")." ".$mes_actual." ".date("Y"),
                    'nombre_contador' => $datosContador->nombre_completo,
                    'contador' => base64_encode($datosContador->id_user),
                    'mensaje' => '<div class="alert alert-warning" role="alert">Hubo un error al asignar al contador, por favor, contacte al administrador.</div>',
                );

                $this->load->view('front/info_contador_view',$data);
            }
        }
        else {
            $datosContador = $this->contadores_model->getInfoContador($contador);

            $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

            $mes_actual = $this->fecha->getMesActual(date("F"));

            $data = array(
                'usuario' => $nombreUsuario->nombre_completo,
                'fecha' => date("j")." ".$mes_actual." ".date("Y"),
                'nombre_contador' => $datosContador->nombre_completo,
                'contador' => base64_encode($datosContador->id_user),
                'mensaje' => '<div class="alert alert-warning" role="alert">Hubo un error al asignar al contador, por favor, contacte al administrador.</div>',
            );

            $this->load->view('front/info_contador_view',$data);
        }

    }

    public function infoContador() {
        $url = $this->uri->segment(3);

        $datosContador = $this->contadores_model->getInfoContador($url);

        $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

        $mes_actual = $this->fecha->getMesActual(date("F"));

        $data = array(
            'usuario' => $nombreUsuario->nombre_completo,
            'fecha' => date("j")." ".$mes_actual." ".date("Y"),
            'nombre_contador' => $datosContador->nombre_completo,
            'contador' => base64_encode($datosContador->id_user),
        );
        $this->load->view('front/info_contador_view',$data);
    }

    public function listaContadores() {
        $contadores = $this->contadores_model->getListaContadores();
        echo(json_encode($contadores));
    }

    public function modificar_perfil() {

        $data = array();

        $post_array = $this->input->post();
        foreach($post_array as $key => $value ) {
            if(trim($value)!= "") {
                $data[$key] = $value;
            }
        }

        $this->form_validation->set_rules('nombre_completo', 'Nombre_completo', 'trim|xss_clean');
        $this->form_validation->set_rules('rfc', 'RFC', 'trim|xss_clean');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');

        if ($this->form_validation->run()) {
            $resultado = $this->home_model->actualiarDatosUsuario($data);

            if($resultado) {
                $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

                $mes_actual = $this->fecha->getMesActual(date("F"));

                $data = array(
                    'usuario' => $nombreUsuario->nombre_completo,
                    'fecha' => date("j")." ".$mes_actual." ".date("Y"),
                    'mensaje' => '<div class="alert alert-success" role="alert">Exito al actualizar los datos!</div>',
                );
                $this->load->view('front/perfil_view',$data);
            }
            else {
                $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

                $mes_actual = $this->fecha->getMesActual(date("F"));

                $data = array(
                    'usuario' => $nombreUsuario->nombre_completo,
                    'fecha' => date("j")." ".$mes_actual." ".date("Y"),
                    'error' => TRUE,
                );
                $this->load->view('front/perfil_view',$data);
            }
        }
        else {
            $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

            $mes_actual = $this->fecha->getMesActual(date("F"));

            $data = array(
                'usuario' => $nombreUsuario->nombre_completo,
                'fecha' => date("j")." ".$mes_actual." ".date("Y"),
                'error' => TRUE,
            );
            $this->load->view('front/perfil_view',$data);
        }

    }

    public function tu_perfil() {
        $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

        $mes_actual = $this->fecha->getMesActual(date("F"));

        $data = array(
            'usuario' => $nombreUsuario->nombre_completo,
            'fecha' => date("j")." ".$mes_actual." ".date("Y"),
        );
        $this->load->view('front/perfil_view',$data);
    }

    public function historial() {
        $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

        $mes_actual = $this->fecha->getMesActual(date("F"));

        $datos = array(
            'usuario' => $nombreUsuario->nombre_completo,
            'fecha' => date("j")." ".$mes_actual." ".date("Y"),
            'years' => $this->fecha->getAvailableYear($this->tank_auth->get_user_id()),
        );
        $this->load->view('front/historial_view',$datos);
    }

    public function historial_rango() {
        $mes_inicio = $this->input->post('mes_inicio');
        $mes_fin = $this->input->post('mes_fin');
        $year = $this->input->post('year');

        $datos = $this->factura_model->rangoMovimientos($this->tank_auth->get_user_id(), $mes_inicio, $mes_fin, $year);

//        Se declaran las variables que seran el calculo total de los dos valores necesarios para poder sacar las utilidades
        $total_ingresos = 0;
        $total_egreso = 0;

//        Este ciclo lo que hace es:
//          1) Revisa que tipo de comprobante es
//          2) En caso de ser "ingreso, se suma a la variable donde va a estar el total de los ingresos
//          3) En caso de ser gasto, se suma a la variable donde va a estar el total de los gastos
//        Si no se cumple ninguno de los dos casos, se dejan las variables en 0
        foreach ($datos as $row) {
            if($row->tipo_comprobante == "ingreso") {
                $total_ingresos += $row->subtotal;
            }
            elseif($row->tipo_comprobante == "gasto"){
                $total_egreso += $row->subtotal;
            }
            else {
                $total_ingresos = 0;
                $total_egreso = 0;
            }
        }

        $crud = new grocery_CRUD();

        $usuario = $this->tank_auth->get_user_id();

        $crud->where('id_user',$usuario);
        $crud->where('fecha >=',$year."-".$mes_inicio."-01");
        $crud->where('fecha <=',$year."-".($mes_fin+1)."-01");
        $crud->set_table('comprobante');
        $crud->columns('no_comprobante','nombre_cliente', 'fecha','tipo_comprobante','metodo_pago','subtotal','tasa_iva', 'iva', 'total', 'pagado', 'fecha_pago');
        $crud->display_as('no_comprobante','Número de comprobante')
            ->display_as('nombre_cliente','Cliente / Proveedor')
            ->display_as('fecha','Fecha')
            ->display_as('tipo_comprobante','Tipo de Comprobante')
            ->display_as('metodo_pago','Metodo de pago')
            ->display_as('subtotal','Subtotal')
            ->display_as('tasa_iva','% IVA')
            ->display_as('iva','IVA')
            ->display_as('total','Total')
            ->display_as('pagado','Pagado')
            ->display_as('fecha_pago','Fecha de Pago');

        $crud->set_subject('Comprobante');

        $crud->callback_column('subtotal',array($this,'formatoNumero'));
        $crud->callback_column('total',array($this,'formatoNumero'));
        $crud->callback_column('pagado',array($this,'formatoPagado'));
        $crud->callback_column('tasa_iva',array($this,'formatoTasaIVA'));
        $crud->callback_column('iva',array($this,'formatoIVA'));
        $crud->callback_column('tipo_comprobante',array($this,'tipoDeComprobante'));

        $crud->callback_before_delete(array($this,'borrarArchivo'));

        $crud->add_action('Ver Informacion de Comprobante', base_url("img/paper18.png"), 'dashboard/verComprobante');


        $crud->unset_read();
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_edit();

        $crud->order_by('fecha','asc');

        $output = $crud->render();

        $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

        $mes_actual = $this->fecha->getMesActual(date("F"));

        $output->usuario = $nombreUsuario->nombre_completo;
        $output->rango = "<b id='mesHistorial'>Del mes:</b> ".$this->fecha->getMesNumero($mes_inicio)." <b id='mesHistorial'>al Mes:</b> ".$this->fecha->getMesNumero($mes_fin)." <b id='mesHistorial'>Año: </b>".$year;
        $output->ingresos = $total_ingresos;
        $output->egresos = $total_egreso;
        $output->fecha = date("j")." ".$mes_actual." ".date("Y");
        $output->years = $this->fecha->getAvailableYear($this->tank_auth->get_user_id());

        $this->historialRango_output($output);
    }

    public function historial_mes() {
        $mes = $this->input->post('mes');
        $year = $this->input->post('year');

        $dt = DateTime::createFromFormat('!m', $mes);
        $mes_convertido = $dt->format('F');

        $datos = $this->factura_model->calculoMovimientos($this->tank_auth->get_user_id(), $mes_convertido, $year);

//        Se declaran las variables que seran el calculo total de los dos valores necesarios para poder sacar las utilidades
        $total_ingresos = 0;
        $total_egreso = 0;

//        Este ciclo lo que hace es:
//          1) Revisa que tipo de comprobante es
//          2) En caso de ser "ingreso, se suma a la variable donde va a estar el total de los ingresos
//          3) En caso de ser gasto, se suma a la variable donde va a estar el total de los gastos
//        Si no se cumple ninguno de los dos casos, se dejan las variables en 0
        foreach ($datos as $row) {
            if($row->tipo_comprobante == "ingreso") {
                $total_ingresos += $row->subtotal;
            }
            elseif($row->tipo_comprobante == "gasto"){
                $total_egreso += $row->subtotal;
            }
            else {
                $total_ingresos = 0;
                $total_egreso = 0;
            }
        }

        $crud = new grocery_CRUD();

        $usuario = $this->tank_auth->get_user_id();

        $crud->where('id_user',$usuario);
        $crud->where('fecha >=',$year."-".$mes."-01");
        $crud->where('fecha <=',$year."-".($mes+1)."-01");
        $crud->set_table('comprobante');
        $crud->columns('no_comprobante','nombre_cliente', 'fecha','tipo_comprobante','metodo_pago','subtotal','tasa_iva', 'iva', 'total', 'pagado', 'fecha_pago');
        $crud->display_as('no_comprobante','Número de comprobante')
            ->display_as('nombre_cliente','Cliente / Proveedor')
            ->display_as('fecha','Fecha')
            ->display_as('tipo_comprobante','Tipo de Comprobante')
            ->display_as('metodo_pago','Metodo de pago')
            ->display_as('subtotal','Subtotal')
            ->display_as('tasa_iva','% IVA')
            ->display_as('iva','IVA')
            ->display_as('total','Total')
            ->display_as('pagado','Pagado')
            ->display_as('fecha_pago','Fecha de Pago');

        $crud->set_subject('Comprobante');

        $crud->callback_column('subtotal',array($this,'formatoNumero'));
        $crud->callback_column('total',array($this,'formatoNumero'));
        $crud->callback_column('pagado',array($this,'formatoPagado'));
        $crud->callback_column('tasa_iva',array($this,'formatoTasaIVA'));
        $crud->callback_column('iva',array($this,'formatoIVA'));
        $crud->callback_column('tipo_comprobante',array($this,'tipoDeComprobante'));

        $crud->callback_before_delete(array($this,'borrarArchivo'));

        $crud->add_action('Ver Informacion de Comprobante', base_url("img/paper18.png"), 'dashboard/verComprobante');


        $crud->unset_read();
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_edit();

        $output = $crud->render();

        $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

        $mes_actual = $this->fecha->getMesActual(date("F"));

        $output->usuario = $nombreUsuario->nombre_completo;
        $output->rango = "<b id='mesHistorial'>Mes:</b> ".$this->fecha->getMesNumero($mes)." <b id='mesHistorial'>Año: </b>".$year;
        $output->ingresos = $total_ingresos;
        $output->egresos = $total_egreso;
        $output->fecha = date("j")." ".$mes_actual." ".date("Y");
        $output->years = $this->fecha->getAvailableYear($this->tank_auth->get_user_id());

        $this->historialMes_output($output);
    }

    public function revisarMes($mes_revisar, $year) {
//        Se llama a la funcion "calculoMovimientos" del modelo "Factura model" y se le pasa el id del usuario logueado como variable de entrada
//        Esta funcion devuelve el subtotal, que tipo de comprobante y si ya esta pagado, para poder hacer el calculo de las utilidadees del cliente
        $datos = $this->factura_model->calculoMovimientos($this->tank_auth->get_user_id(), $mes_revisar, $year);

//        Se declaran las variables que seran el calculo total de los dos valores necesarios para poder sacar las utilidades
        $total_ingresos = 0;
        $total_egreso = 0;

//        Este ciclo lo que hace es:
//          1) Revisa que tipo de comprobante es
//          2) En caso de ser "ingreso, se suma a la variable donde va a estar el total de los ingresos
//          3) En caso de ser gasto, se suma a la variable donde va a estar el total de los gastos
//        Si no se cumple ninguno de los dos casos, se dejan las variables en 0
        foreach ($datos as $row) {
            if($row->tipo_comprobante == "ingreso") {
                $total_ingresos += $row->subtotal;
            }
            elseif($row->tipo_comprobante == "gasto"){
                $total_egreso += $row->subtotal;
            }
            else {
                $total_ingresos = 0;
                $total_egreso = 0;
            }
        }

//        Se toma el subtotal, mes y tipo de comprobante del cliente, para poder hacer el calculo de las utilidades del cliente, por año
        $impuestos = $this->factura_model->tomarMeses($this->tank_auth->get_user_id());

//        Se declara el arreglo que va a tener el calculo de utilidades del cliente por mes
        $impuestos_mes = array(
            'enero' => 0.0,
            'febrero' => 0.0,
            'marzo' => 0.0,
            'abril' => 0.0,
            'mayo' => 0.0,
            'junio' => 0.0,
            'julio' => 0.0,
            'agosto' => 0.0,
            'septiembre' => 0.0,
            'octubre' => 0.0,
            'noviembre' => 0.0,
            'diciembre' => 0.0,
        );


//        Se declara el arreglo que va a tener el total de los ingreso del cliente por mes
        $ingresos_mes = array(
            0 => 0.0,
            1 => 0.0,
            2 => 0.0,
            3 => 0.0,
            4 => 0.0,
            5 => 0.0,
            6 => 0.0,
            7 => 0.0,
            8 => 0.0,
            9 => 0.0,
            10 => 0.0,
            11 => 0.0,
        );

//        Se declara el arreglo que va a tener el total de los egreso del cliente por mes
        $egresos_mes = array(
            0 => 0.0,
            1 => 0.0,
            2 => 0.0,
            3 => 0.0,
            4 => 0.0,
            5 => 0.0,
            6 => 0.0,
            7 => 0.0,
            8 => 0.0,
            9 => 0.0,
            10 => 0.0,
            11 => 0.0,
        );

//        Este ciclo lo que hace es:
//          1) Se ve por fila del arreglo, que tipo de mes es
//          2) Una ves que se determina el mes, se revisa que tipo de comprobante es
//          3) En caso de ser ingreso, se suma al arreglo en la posicion que le corresponde
//          4) En case de ser gasto, se resta al arreglo en la posicion que le corresponde
        foreach($impuestos as $row ){
            switch($row->mes_pago){
                case "January":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['enero'] += $row->subtotal;
                        $ingresos_mes[0] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['enero'] -= $row->subtotal;
                        $egresos_mes[0] -= $row->subtotal;
                    }
                    break;
                case "February":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['febrero'] += $row->subtotal;
                        $ingresos_mes[1] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['febrero'] -= $row->subtotal;
                        $egresos_mes[1] -= $row->subtotal;
                    }
                    break;
                case "March":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['marzo'] += $row->subtotal;
                        $ingresos_mes[2] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['marzo'] -= $row->subtotal;
                        $egresos_mes[2] -= $row->subtotal;
                    }
                    break;
                case "April":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['abril'] += $row->subtotal;
                        $ingresos_mes[3] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['abril'] -= $row->subtotal;
                        $egresos_mes[3] -= $row->subtotal;
                    }
                    break;
                case "May":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['mayo'] += $row->subtotal;
                        $ingresos_mes[4] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['mayo'] -= $row->subtotal;
                        $egresos_mes[4] -= $row->subtotal;
                    }
                    break;
                case "June":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['junio'] += $row->subtotal;
                        $ingresos_mes[5] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['junio'] -= $row->subtotal;
                        $egresos_mes[5] -= $row->subtotal;
                    }
                    break;
                case "July":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['julio'] += $row->subtotal;
                        $ingresos_mes[6] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['julio'] -= $row->subtotal;
                        $egresos_mes[6] -= $row->subtotal;
                    }
                    break;
                case "August":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['agosto'] += $row->subtotal;
                        $ingresos_mes[7] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['agosto'] -= $row->subtotal;
                        $egresos_mes[7] -= $row->subtotal;
                    }
                    break;
                case "September":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['septiembre'] += $row->subtotal;
                        $ingresos_mes[8] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['septiembre'] -= $row->subtotal;
                        $egresos_mes[8] -= $row->subtotal;
                    }
                    break;
                case "October":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['octubre'] += $row->subtotal;
                        $ingresos_mes[9] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['octubre'] -= $row->subtotal;
                        $egresos_mes[9] -= $row->subtotal;
                    }
                    break;
                case "November":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['noviembre'] += $row->subtotal;
                        $ingresos_mes[10] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['noviembre'] -= $row->subtotal;
                        $egresos_mes[10] -= $row->subtotal;
                    }
                    break;
                case "December":
                    if($row->tipo_comprobante == 'ingreso') {
                        $impuestos_mes['diciembre'] += $row->subtotal;
                        $ingresos_mes[11] += $row->subtotal;
                    }
                    elseif($row->tipo_comprobante == 'gasto') {
                        $impuestos_mes['diciembre'] -= $row->subtotal;
                        $egresos_mes[11] -= $row->subtotal;
                    }
                    break;
            }
        }

//        Se declara la variable que va a tener el total de la utilidad del cliente por mes
        $resultado_utilidades = array();

//        Este ciclo lo que hace es:
//          1) Se por fila del arreglo, que tipo de mes es
//          2) Una ves que se determina el mes, se revisa que tipo de comprobante es
//          3) En caso de ser ingreso, se suma al arreglo en la posicion que le corresponde
//          4) En case de ser gasto, se resta al arreglo en la posicion que le corresponde
        foreach($impuestos_mes as $mes) {
            $resultado_utilidades[] = $mes;
        }

        $js_array = json_encode($resultado_utilidades);

        $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

        $iva = $this->factura_model->calcularIvaPorPagar();
        $iva_a_pagar = 0.0;
        if(!empty($iva)) {
            foreach($iva as $info) {
                if($info->tipo_comprobante == 'ingreso') {
                    $iva_a_pagar += $info->iva;
                }
                else {
                    $iva_a_pagar -= $info->iva;
                }
            }
        }

        $data = array(
            'usuario' => $nombreUsuario->nombre_completo,
            'mes' => $this->getMesActual($mes_revisar),
            'por_cobrar' => $this->getCuentasCobrar(),
            'por_pagar' => $this->getCuentasPagar(),
            'ingresos' => $total_ingresos,
            'egresos' => $total_egreso,
            'utilidad_mes' => $total_ingresos - $total_egreso,
            'utilidades_year' => $js_array,
            'ingresos_mes' => $ingresos_mes,
            'egresos_mes' => $egresos_mes,
            'iva_por_pagar' => $iva_a_pagar,
        );

        $this->load->view('front/home_view', $data);
    }

    public function contacto(){
        $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

        $data = array(
            'usuario' => $nombreUsuario->nombre_completo,
        );

        $this->load->view('front/contacto_view', $data);
    }

    public function cuentas_pagar($month = NULL){
        $usuario = $this->tank_auth->get_user_id();

        $folderName = './application/uploads/'.$usuario;

        if(!file_exists($folderName)) {
            mkdir($folderName, 0777, true);
        }

        $crud = new grocery_CRUD();

        if($month == NULL) {
            $month = date("F");
        }
        else {
            $month = $this->uri->segment(3);
        }

        $crud->where('id_user',$this->tank_auth->get_user_id());
        $crud->where('mes',$month);
        $crud->where('tipo_comprobante',"gasto");
        $crud->where('pagado',0);
        $crud->set_table('comprobante');
            $crud->columns('no_comprobante','fecha','mes','subtotal','moneda','total','tipo_comprobante', 'metodo_pago', 'tasa_iva', 'iva',  'pagado', 'fecha_pago');
        $crud->display_as('no_comprobante','Número de comprobante')
            ->display_as('fecha','Fecha')
            ->display_as('mes','Mes')
            ->display_as('subtotal','Subtotal')
            ->display_as('moneda','Moneda')
            ->display_as('total','Total')
            ->display_as('tipo_comprobante','Tipo de Comprobante')
            ->display_as('metodo_pago','Metodo de pago')
            ->display_as('pagado','Pagado');
        $crud->set_subject('Por Pagar');

        $crud->callback_column('subtotal',array($this,'formatoNumero'));
        $crud->callback_column('total',array($this,'formatoNumero'));
        $crud->callback_column('pagado',array($this,'formatoPagado'));
        $crud->callback_column('tasa_iva',array($this,'formatoTasaIVA'));
        $crud->callback_column('iva',array($this,'formatoIVA'));

        $crud->callback_after_update(array($this,'insertarFechaPago'));

        $crud->set_field_upload('archivo',$folderName);

        $crud->add_fields('no_comprobante',
            'fecha',
            'subtotal',
            'moneda',
            'total',
            'metodo_pago',
            'rfc_de_emisor',
            'nombre_de_emisor',
            'calle_de_emisor',
            'no_exterior_de_emisor',
            'colonia_de_emisor',
            'municipio_de_emisor',
            'estado_de_emisor',
            'pais_de_emisor',
            'codigo_postal_de_emisor',
            'pagado',
            'archivo');
        $crud->edit_fields('fecha_pago');
        $crud->unset_add();

        $crud->add_action('Ver Informacion de Comprobante', base_url("img/paper18.png"), 'dashboard/verComprobante');

        $output = $crud->render();

        $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

        $output->usuario = $nombreUsuario->nombre_completo;
        $output->mesactivo = $month;

        $this->cuentas_pagar_output($output);
    }

    public function cuentas_cobrar(){
        $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

        $data = array(
            'usuario' => $nombreUsuario->nombre_completo,
        );

        $this->load->view('front/porCobrar_view', $data);
    }

    public function datos_persona_cobro($rfc_persona = NULL) {
        $crud = new grocery_CRUD();

        $usuario = $this->tank_auth->get_user_id();

        $folderName = './application/uploads/'.$usuario;

        if(!file_exists($folderName)) {
            mkdir($folderName, 0777, true);
        }

        $crud->where('rfc_cliente',$rfc_persona );
        $crud->where('id_user',$usuario);
        $crud->set_table('comprobante');
        $crud->columns('no_comprobante','nombre_cliente', 'fecha','tipo_comprobante','metodo_pago','subtotal','tasa_iva', 'iva', 'total', 'pagado', 'fecha_pago');
        $crud->display_as('no_comprobante','Número de comprobante')
            ->display_as('nombre_cliente','Cliente / Proveedor')
            ->display_as('fecha','Fecha')
            ->display_as('tipo_comprobante','Tipo de Comprobante')
            ->display_as('metodo_pago','Metodo de pago')
            ->display_as('subtotal','Subtotal')
            ->display_as('tasa_iva','% IVA')
            ->display_as('iva','IVA')
            ->display_as('total','Total')
            ->display_as('pagado','Pagado')
            ->display_as('fecha_pago','Fecha de Pago');

        $crud->set_subject('Comprobante');

        $crud->callback_column('subtotal',array($this,'formatoNumero'));
        $crud->callback_column('total',array($this,'formatoNumero'));
        $crud->callback_column('pagado',array($this,'formatoPagado'));
        $crud->callback_column('tasa_iva',array($this,'formatoTasaIVA'));
        $crud->callback_column('iva',array($this,'formatoIVA'));

        $crud->callback_after_update(array($this,'insertarFechaPago'));
        $crud->callback_before_delete(array($this,'borrarArchivo'));

        $crud->set_field_upload('archivo',$folderName);

        $crud->add_fields('no_comprobante',
            'fecha',
            'subtotal',
            'moneda',
            'total',
            'metodo_pago',
            'rfc_de_emisor',
            'nombre_de_emisor',
            'calle_de_emisor',
            'no_exterior_de_emisor',
            'colonia_de_emisor',
            'municipio_de_emisor',
            'estado_de_emisor',
            'pais_de_emisor',
            'codigo_postal_de_emisor',
            'pagado',
            'archivo');
        $crud->edit_fields('fecha_pago');

        $crud->add_action('Ver Informacion de Comprobante', base_url("img/paper18.png"), 'dashboard/verComprobante');
        $crud->unset_add();
        $crud->unset_edit();
        $crud->unset_delete();
        $crud->unset_read();

        $output = $crud->render();

        $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

        $output->usuario = $nombreUsuario->nombre_completo;
        $output->tipo_comprobante = "ingreso";

        $this->datos_persona_cobro_output($output);
    }

    public function datos_persona($rfc_persona = NULL) {
        $crud = new grocery_CRUD();

        $usuario = $this->tank_auth->get_user_id();

        $folderName = './application/uploads/'.$usuario;

        if(!file_exists($folderName)) {
            mkdir($folderName, 0777, true);
        }

        $crud->where('rfc_cliente',$rfc_persona );
        $crud->where('id_user',$usuario);
        $crud->set_table('comprobante');
        $crud->columns('no_comprobante','nombre_cliente', 'fecha','tipo_comprobante','metodo_pago','subtotal','tasa_iva', 'iva', 'total', 'pagado', 'fecha_pago');
        $crud->display_as('no_comprobante','Número de comprobante')
            ->display_as('nombre_cliente','Cliente / Proveedor')
            ->display_as('fecha','Fecha')
            ->display_as('tipo_comprobante','Tipo de Comprobante')
            ->display_as('metodo_pago','Metodo de pago')
            ->display_as('subtotal','Subtotal')
            ->display_as('tasa_iva','% IVA')
            ->display_as('iva','IVA')
            ->display_as('total','Total')
            ->display_as('pagado','Pagado')
            ->display_as('fecha_pago','Fecha de Pago');

        $crud->set_subject('Comprobante');

        $crud->callback_column('subtotal',array($this,'formatoNumero'));
        $crud->callback_column('total',array($this,'formatoNumero'));
        $crud->callback_column('pagado',array($this,'formatoPagado'));
        $crud->callback_column('tasa_iva',array($this,'formatoTasaIVA'));
        $crud->callback_column('iva',array($this,'formatoIVA'));

        $crud->callback_after_update(array($this,'insertarFechaPago'));
        $crud->callback_before_delete(array($this,'borrarArchivo'));

        $crud->set_field_upload('archivo',$folderName);

        $crud->add_fields('no_comprobante',
            'fecha',
            'subtotal',
            'moneda',
            'total',
            'metodo_pago',
            'rfc_de_emisor',
            'nombre_de_emisor',
            'calle_de_emisor',
            'no_exterior_de_emisor',
            'colonia_de_emisor',
            'municipio_de_emisor',
            'estado_de_emisor',
            'pais_de_emisor',
            'codigo_postal_de_emisor',
            'pagado',
            'archivo');
        $crud->edit_fields('fecha_pago');

        $crud->add_action('Ver Informacion de Comprobante', base_url("img/paper18.png"), 'dashboard/verComprobante');
        $crud->unset_add();
        $crud->unset_edit();
        $crud->unset_delete();
        $crud->unset_read();

        $output = $crud->render();

        $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

        $output->usuario = $nombreUsuario->nombre_completo;
        $output->tipo_comprobante = "gasto";

        $this->datos_persona_output($output);
    }

    public function comprobantes($month = NULL, $error = NULL) {

        $crud = new grocery_CRUD();

        $usuario = $this->tank_auth->get_user_id();

        $folderName = './application/uploads/'.$usuario;

        if(!file_exists($folderName)) {
            mkdir($folderName, 0777, true);
        }

        $crud->where('mes',$month);
        $crud->where('id_user',$usuario);
        $crud->set_table('comprobante');
        $crud->columns('no_comprobante','nombre_cliente', 'fecha','tipo_comprobante','metodo_pago','subtotal','tasa_iva', 'iva', 'total', 'pagado', 'fecha_pago');
        $crud->display_as('no_comprobante','Número de comprobante')
            ->display_as('nombre_cliente','Cliente / Proveedor')
            ->display_as('fecha','Fecha')
            ->display_as('tipo_comprobante','Tipo de Comprobante')
            ->display_as('metodo_pago','Metodo de pago')
            ->display_as('subtotal','Subtotal')
            ->display_as('tasa_iva','% IVA')
            ->display_as('iva','IVA')
            ->display_as('total','Total')
            ->display_as('pagado','Pagado')
            ->display_as('fecha_pago','Fecha de Pago');

        $crud->set_subject('Comprobante');

        $crud->callback_column('subtotal',array($this,'formatoNumero'));
        $crud->callback_column('total',array($this,'formatoNumero'));
        $crud->callback_column('pagado',array($this,'formatoPagado'));
        $crud->callback_column('tasa_iva',array($this,'formatoTasaIVA'));
        $crud->callback_column('iva',array($this,'formatoIVA'));
        $crud->callback_column('tipo_comprobante',array($this,'tipoDeComprobante'));

        $crud->callback_before_delete(array($this,'borrarArchivo'));

        $crud->set_field_upload('archivo',$folderName);

        $crud->add_fields('no_comprobante',
            'fecha',
            'subtotal',
            'moneda',
            'total',
            'metodo_pago',
            'rfc_de_emisor',
            'nombre_de_emisor',
            'calle_de_emisor',
            'no_exterior_de_emisor',
            'colonia_de_emisor',
            'municipio_de_emisor',
            'estado_de_emisor',
            'pais_de_emisor',
            'codigo_postal_de_emisor',
            'pagado',
            'archivo');
        $crud->edit_fields('fecha_pago');

        $crud->add_action('Ver Informacion de Comprobante', base_url("img/paper18.png"), 'dashboard/verComprobante');

        $crud->unset_read();

        $output = $crud->render();

        $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

        $mes_actual = $this->fecha->getMesActual(date("F"));

        $output->usuario = $nombreUsuario->nombre_completo;
        $output->error = $error;
        $output->mesactivo = $month;
        $output->fecha = date("j")." ".$mes_actual." ".date("Y");

        $this->comprobantes_output($output);
    }

    public function todosComprobantes($error = NULL) {
        $crud = new grocery_CRUD();

        $usuario = $this->tank_auth->get_user_id();

        $folderName = './application/uploads/'.$usuario;

        if(!file_exists($folderName)) {
            mkdir($folderName, 0777, true);
        }

        $crud->where('id_user',$usuario);
        $crud->set_table('comprobante');
        $crud->columns('no_comprobante','nombre_cliente', 'fecha','tipo_comprobante','metodo_pago','subtotal','tasa_iva', 'iva', 'total', 'pagado', 'fecha_pago');
        $crud->display_as('no_comprobante','Número de comprobante')
            ->display_as('nombre_cliente','Cliente / Proveedor')
            ->display_as('fecha','Fecha')
            ->display_as('tipo_comprobante','Tipo de Comprobante')
            ->display_as('metodo_pago','Metodo de pago')
            ->display_as('subtotal','Subtotal')
            ->display_as('tasa_iva','% IVA')
            ->display_as('iva','IVA')
            ->display_as('total','Total')
            ->display_as('pagado','Pagado')
            ->display_as('fecha_pago','Fecha de Pago');

        $crud->set_subject('Comprobante');

        $crud->callback_column('subtotal',array($this,'formatoNumero'));
        $crud->callback_column('total',array($this,'formatoNumero'));
        $crud->callback_column('pagado',array($this,'formatoPagado'));
        $crud->callback_column('tasa_iva',array($this,'formatoTasaIVA'));
        $crud->callback_column('iva',array($this,'formatoIVA'));
        $crud->callback_column('tipo_comprobante',array($this,'tipoDeComprobante'));

        $crud->callback_before_delete(array($this,'borrarArchivo'));

        $crud->set_field_upload('archivo',$folderName);

        $crud->add_fields('no_comprobante',
            'fecha',
            'subtotal',
            'moneda',
            'total',
            'metodo_pago',
            'rfc_de_emisor',
            'nombre_de_emisor',
            'calle_de_emisor',
            'no_exterior_de_emisor',
            'colonia_de_emisor',
            'municipio_de_emisor',
            'estado_de_emisor',
            'pais_de_emisor',
            'codigo_postal_de_emisor',
            'pagado',
            'archivo');
        $crud->edit_fields('fecha_pago');

        $crud->add_action('Ver Informacion de Comprobante', base_url("img/paper18.png"), 'dashboard/verComprobante');

        $output = $crud->render();

        $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

        $mes_actual = $this->fecha->getMesActual(date("F"));

        $output->usuario = $nombreUsuario->nombre_completo;
        $output->error = $error;
        $output->fecha = date("j")." ".$mes_actual." ".date("Y");

        $this->todocomprobantes_output($output);
    }

    public function borrarArchivo($primary_key) {
        $this->db->select('archivo');
        $this->db->where('id_factura',$primary_key);

        $comprobante = $this->db->get('comprobante')->row();

        if(empty($comprobante))
            return false;

        unlink($comprobante->archivo);
        return true;
    }

    public function verComprobante($id) {
        $nombreUsuario = $this->fecha->getNombre($this->tank_auth->get_user_id());

        $datos["usuario"] = $nombreUsuario->nombre_completo;
        $datos["id_comprobante"] = $id;

        $this->load->view('front/comprobante_view', $datos);
    }

    public function datos_persona_output($output = null){
        $this->load->view('front/datosPersona_view.php',$output);
    }

    public function datos_persona_cobro_output($output = null){
        $this->load->view('front/datosPersonaCobro_view.php',$output);
    }

    public function comprobantes_output($output = null) {
        $this->load->view('front/todoscomprobantes_view.php',$output);
    }

    public function todocomprobantes_output($output = null) {
        $this->load->view('front/todoscomprobantes_view.php',$output);
    }

    public function historialMes_output($output = null) {
        $this->load->view('front/historialMes_view.php',$output);
    }

    public function historialRango_output($output = null) {
        $this->load->view('front/historialRango_view.php',$output);
    }

    public function cuentas_pagar_output($output = null) {
        $this->load->view('front/porPagar_view.php',$output);
    }

    public function formatoNumero($value, $row) {
        return "$".number_format($value,2,".",",");
    }

    public function formatoPagado($value, $row) {
        $link = $this->uri->segment(2);
        if($value == 0) {
            return "<a href=".base_url('dashboard/marcarPagado/'.$row->id_factura.'/'.$row->mes.'/'.$link).">No</a>";
        }
        elseif($value == 1) {
            return "<a href=".base_url('dashboard/desmarcarPagado/'.$row->id_factura.'/'.$row->mes.'/'.$link)."><img src='".base_url('img/check24-16.png')."' /></a>";
        }
    }

    public function formatoTasaIVA($value, $row) {
        return "%".number_format($value,0,".",",");
    }

    public function formatoIVA($value, $row) {
        return "$".number_format($value,2,".",",");
    }

    public function tipoDeComprobante($value, $row) {
//        if($value == 'ingreso') {
//            return "<p style='color: green;'>".$value."</p>";
//        }
//        elseif($value == 'gasto') {
//            return "<p style='color: red;'>".$value."</p>";
//        }
        return "<p class='".($value=='ingreso'?'ingreso':'gasto')."'>$value</p>";
    }

    public function desmarcarPagado($id, $mes, $link) {
        $data = array(
            'pagado' => 0,
            'fecha_pago' => NULL,
            'mes_pago' => NULL,
        );

        $this->db->where('id_factura', $id);
        $query = $this->db->update('comprobante', $data);
        if($query){
            redirect('dashboard/'.$link.'/'.$mes);
        }
        else {
            return false;
        }
    }

    public function marcarPagado($id, $mes, $link) {
        $data = array(
            'pagado' => 1,
            'fecha_pago' => date("Y-m-d"),
            'mes_pago' => date("F"),
        );

        $this->db->where('id_factura', $id);
        $query = $this->db->update('comprobante', $data);
        if($query){
            redirect('dashboard/'.$link.'/'.$mes);
        }
        else {
            return false;
        }
    }

    public function subir_archivo($link){
        $usuario = $this->tank_auth->get_user_id();
        $alerta = '';

        $folderName = './application/uploads/'.$usuario;

        if(!file_exists($folderName)) {
            mkdir($folderName, 0777, true);
        }

        $this->load->library('upload');

        $this->upload->initialize(array(
            "upload_path"   => $folderName,
            "allowed_types"   => "*",
        ));

        if($this->upload->do_multi_upload("files")) {
            $datos = $this->upload->get_multi_upload_data();
            foreach($datos as $dato) {
                if($dato['file_ext'] == ".xml" || $dato['file_ext'] == ".XML") {
                    $doc = new DOMDocument();
                    $doc->load( $dato['full_path'] );

                    $comprobante = $doc->getElementsByTagName( "Comprobante" );

                    foreach( $comprobante as $elemento ) {
                        setlocale(LC_MONETARY, 'es_MX');
                        $tipo_comprobante = '';
                        $fecha = $elemento->getAttribute('fecha');
                        $fecha = explode('T', $fecha, 2);
                        $nueva_fecha = explode('-', $fecha[0]);
                        $numero_comprobante = $elemento->getAttribute('serie').$elemento->getAttribute('folio');
                        $total = $elemento->getAttribute('total');
                        $moneda= $elemento->getAttribute('Moneda');
                        $subtotal= $elemento->getAttribute('subTotal');
                        $metodoPago= $elemento->getAttribute('metodoDePago');
                        $total_formato = number_format($total,2,".",",");

                        $emisor = $doc->getElementsByTagName( "Emisor" );
                        $rfc_emisor = $emisor->item(0)->getAttribute('rfc');

                        if($this->revisarRFC($rfc_emisor) == TRUE){
                            $tipo_comprobante = 'ingreso';
                        }
                        else {
                            $tipo_comprobante = 'gasto';
                        }

                        $cliente = $emisor->item(0)->getAttribute('nombre');
                        $iva = $doc->getElementsByTagName( "Traslado" );
                        $valor_iva = $iva->item(0)->getAttribute('importe');
                        $tasa_iva = $iva->item(0)->getAttribute('tasa');

                        $emisor = $doc->getElementsByTagName( "DomicilioFiscal" );
                        $calle_emisor = $emisor->item(0)->getAttribute('calle');
                        $no_exterior_emisor = $emisor->item(0)->getAttribute('noExterior');
                        $colonia_emisor = $emisor->item(0)->getAttribute('colonia');
                        $municipio_emisor = $emisor->item(0)->getAttribute('municipio');
                        $estado_emisor = $emisor->item(0)->getAttribute('estado');
                        $pais_emisor = $emisor->item(0)->getAttribute('pais');
                        $cp_emisor = $emisor->item(0)->getAttribute('codigoPostal');

                        $receptor = $doc->getElementsByTagName( "Receptor" );
                        $rfc_receptor = $receptor->item(0)->getAttribute('rfc');
                        $nombre_receptor = $receptor->item(0)->getAttribute('nombre');

                        $datos_receptor = $doc->getElementsByTagName( "Domicilio" );
                        $calle_receptor = $datos_receptor->item(0)->getAttribute('calle');
                        $numero_exterior_receptor = $datos_receptor->item(0)->getAttribute('noExterior');
                        $colonia_receptor = $datos_receptor->item(0)->getAttribute('colonia');
                        $municipio_receptor = $datos_receptor->item(0)->getAttribute('municipio');
                        $estado_receptor = $datos_receptor->item(0)->getAttribute('estado');
                        $pais_receptor = $datos_receptor->item(0)->getAttribute('pais');
                        $cp_receptor = $datos_receptor->item(0)->getAttribute('codigoPostal');

                        $mes = date("F", mktime(0, 0, 0, $nueva_fecha[1], 10));

                        if($tipo_comprobante == "ingreso") {
                            $datos_comprobante = array(
                                'id_user' => $usuario,
                                'no_comprobante' => $numero_comprobante,
                                'fecha' => $fecha[0],
                                'mes' => $mes,
                                'ano' => $nueva_fecha[0],
                                'subtotal' => $subtotal,
                                'moneda' => $moneda,
                                'total' => $total,
                                'tipo_comprobante' => $tipo_comprobante,
                                'metodo_pago' => $metodoPago,
                                'iva' => $valor_iva,
                                'tasa_iva' => $tasa_iva,
                                'archivo' => $dato['full_path'],
                                'rfc_cliente' => $rfc_receptor,
                                'nombre_cliente' => $nombre_receptor,
                                'calle_cliente' => $calle_receptor,
                                'no_exterior_cliente' => $numero_exterior_receptor,
                                'colonia_cliente' => $colonia_receptor,
                                'municipio_cliente' => $municipio_receptor,
                                'estado_cliente' => $estado_receptor,
                                'pais_cliente' => $pais_receptor,
                                'cp_cliente' => $cp_receptor,
                            );
                        }
                        else {
                            $datos_comprobante = array(
                                'id_user' => $usuario,
                                'no_comprobante' => $numero_comprobante,
                                'fecha' => $fecha[0],
                                'mes' => $mes,
                                'ano' => $nueva_fecha[0],
                                'subtotal' => $subtotal,
                                'moneda' => $moneda,
                                'total' => $total,
                                'tipo_comprobante' => $tipo_comprobante,
                                'metodo_pago' => $metodoPago,
                                'iva' => $valor_iva,
                                'tasa_iva' => $tasa_iva,
                                'archivo' => $dato['full_path'],
                                'rfc_cliente' => $rfc_emisor,
                                'nombre_cliente' => $cliente,
                                'calle_cliente' => $calle_emisor,
                                'no_exterior_cliente' => $no_exterior_emisor,
                                'colonia_cliente' => $colonia_emisor,
                                'municipio_cliente' => $municipio_emisor,
                                'estado_cliente' => $estado_emisor,
                                'pais_cliente' => $pais_emisor,
                                'cp_cliente' => $cp_emisor,
                            );
                        }

                        if($this->factura_model->insertarComprobante($datos_comprobante)) {
                            $alerta = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>¡Exito al subir!</strong></div>';
                        }
                        else {
                            $alerta .= '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>¡Error!</strong> Hubo un error al subir el archivo '.$dato['file_name'].', por favor, inténtelo nuevamente.</div>';
                        }
                    }
                }
            }
            $this->comprobantes(date("F"),$alerta);
        }
        else {
            $alerta .= '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>¡Error!</strong> Hubo un error al subir el archivo '.$dato['file_name'].', por favor, inténtelo nuevamente.</div>';
            $this->comprobantes(date("F"),$alerta);
        }
    }

    private function revisarRFC($revisarRFC) {
        $rfc = $this->factura_model->tomarRFC($this->tank_auth->get_user_id());
        if($rfc == $revisarRFC){
            return true;
        }
        else {
            return false;
        }
    }

    private function getCuentasCobrar(){
        $usuario = $this->tank_auth->get_user_id();
        $sql = "SELECT total FROM comprobante WHERE pagado = 0 AND id_user = ? AND tipo_comprobante = 'ingreso'";
        $result = $this->db->query($sql, array($usuario));
        $row = $result->result();
        $resultado = 0;
        foreach($row as $fila){
            $resultado += $fila->total;
        }
        return $resultado;
    }

    private function getCuentasPagar() {
        $usuario = $this->tank_auth->get_user_id();
        $sql = "SELECT total FROM comprobante WHERE pagado = 0 AND id_user = ? AND tipo_comprobante = 'gasto'";
        $result = $this->db->query($sql, array($usuario));
        $row = $result->result();
        $resultado = 0;
        foreach($row as $fila){
            $resultado += $fila->total;
        }
        return $resultado;
    }
}