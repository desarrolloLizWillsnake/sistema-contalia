<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 */
class Fecha {

    public function __construct() {
        $this->ci =& get_instance();
    }

    public function getMesNumero($mes) {
        $mes_traducido = '';
        switch($mes){
            case '01':
                $mes_traducido = 'Enero';
                break;
            case '02':
                $mes_traducido = 'Febrero';
                break;
            case '03':
                $mes_traducido = 'Marzo';
                break;
            case '04':
                $mes_traducido = 'Abril';
                break;
            case '05':
                $mes_traducido = 'Mayo';
                break;
            case '06':
                $mes_traducido = 'Junio';
                break;
            case '07':
                $mes_traducido = 'Julio';
                break;
            case '08':
                $mes_traducido = 'Agosto';
                break;
            case '09':
                $mes_traducido = 'Septiembre';
                break;
            case '10':
                $mes_traducido = 'Octubre';
                break;
            case '11':
                $mes_traducido = 'Noviembre';
                break;
            case '12':
                $mes_traducido = 'Diciembre';
                break;
        }
        return $mes_traducido;
    }

    public function getMesActual($mes) {
        $mes_traducido = '';
        switch($mes){
            case 'January':
                $mes_traducido = 'Enero';
                break;
            case 'February':
                $mes_traducido = 'Febrero';
                break;
            case 'March':
                $mes_traducido = 'Marzo';
                break;
            case 'April':
                $mes_traducido = 'Abril';
                break;
            case 'May':
                $mes_traducido = 'Mayo';
                break;
            case 'June':
                $mes_traducido = 'Junio';
                break;
            case 'July':
                $mes_traducido = 'Julio';
                break;
            case 'August':
                $mes_traducido = 'Agosto';
                break;
            case 'September':
                $mes_traducido = 'Septiembre';
                break;
            case 'October':
                $mes_traducido = 'Octubre';
                break;
            case 'November':
                $mes_traducido = 'Noviembre';
                break;
            case 'December':
                $mes_traducido = 'Diciembre';
                break;
        }
        return $mes_traducido;
    }

    public function getNombre($usuario){
        $this->ci->db->select('nombre_completo');
        $this->ci->db->where('id_user', $usuario);
        $query = $this->ci->db->get('datos');
        return $query->row();
    }

    public function getDatosCliente($usuario) {
        $this->ci->db->select('*');
        $this->ci->db->where('id_user', $usuario);
        $query = $this->ci->db->get('datos');
        return $query->row();
    }

    public function getAvailableYear($usuario) {
        $this->ci->db->select('ano');
        $this->ci->db->where('id_user', $usuario);
        $this->ci->db->group_by("ano");
        $query = $this->ci->db->get('comprobante');
        return $query->result();
    }
}