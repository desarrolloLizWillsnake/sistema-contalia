<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contadores_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function verificarContador($key) {
        $this->db->where('id_user',$key);
        $query = $this->db->get('roles');
        if ($query->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    //    Esta funcion se encarga de regresar el subtotal y tipos de comprobantes del usuario que ya estan pagados, del mes en curso
    public function getListaContadores() {
        $this->db->select('id');
        $this->db->where('tipo_usuario', 2);
        $query = $this->db->get('users');
        $row = $query->result();
        $contadores = array();
        foreach($row as $fila) {
            $this->db->select('nombre_completo');
            $this->db->where('id_user', $fila->id);
            $query2 = $this->db->get('datos');
            $row2 = $query2->row();
            $contadores[$fila->id] = $row2->nombre_completo;
        }
        return $contadores;
    }

    public function getInfoContador($id) {
        $this->db->select('*');
        $this->db->where('id_user', $id);
        $query = $this->db->get('datos');
        $row = $query->row();
        return $row;
    }

    public function setContador($idContador, $idUsuario) {
        $data = array(
            'id_user' => $idUsuario,
            'id_contador' => $idContador,
        );

        $query = $this->db->insert('contador_usuario', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function notificarContador($id_user, $id_contador, $mensaje) {
        $data = array(
            'id_user' => $id_user,
            'id_contador' => $id_contador,
            'mensaje' => $mensaje,
        );

        $query = $this->db->insert('notificaciones', $data);

        if($query) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

}