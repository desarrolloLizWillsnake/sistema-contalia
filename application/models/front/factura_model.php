<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Factura_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    //    Esta funcion se encarga de regresar el subtotal y tipos de comprobantes del usuario que ya estan pagados, del mes en curso
    public function rangoMovimientos($id, $mes_inicio, $mes_fin, $year) {
        $sql = "SELECT subtotal, tipo_comprobante FROM comprobante WHERE id_user = ? AND fecha >= ? AND fecha <= ? AND ano = ? AND pagado = 1";
        $result = $this->db->query($sql, array($id, $year."-".$mes_inicio."-01", $year."-".($mes_fin+1)."-01", $year));
        $row = $result->result();
        return $row;
    }

//    Esta funcion se encarga de regresar el subtotal y tipos de comprobantes del usuario que ya estan pagados, del mes en curso
    public function calculoMovimientos($id, $mes, $year) {
        $sql = "SELECT subtotal, tipo_comprobante FROM comprobante WHERE id_user = ? AND mes_pago = ? AND ano = ? AND pagado = 1";
        $result = $this->db->query($sql, array($id, $mes, $year));
        $row = $result->result();
        return $row;
    }

    public function seleccionDeMeses() {
        $sql = "SELECT mes FROM comprobante GROUP BY MONTH(fecha)";
        $result = $this->db->query($sql);
        $row = $result->result();
        return $row;
    }

    public function calculoAnualIngresos($id) {
        $sql = "SELECT total, mes FROM comprobante WHERE id_user = ? AND tipo_comprobante = 'ingreso' ORDER BY MONTH(fecha)";
        $result = $this->db->query($sql, array($id));
        $row = $result->result();
        return $row;
    }

//    Esta funcion toma el subtotal, mes y tipo de comprobante del cliente que esten pagados y los ordena por mes
    public function tomarMeses($id) {
        $sql = "SELECT subtotal, mes_pago, tipo_comprobante FROM comprobante WHERE id_user = ? AND pagado = 1 ORDER BY MONTH(fecha_pago)";
        $result = $this->db->query($sql, array($id));
        $row = $result->result();
        return $row;
    }

    public function calcularIvaPorPagar() {
        $id = $this->tank_auth->get_user_id();
        $sql = "SELECT tipo_comprobante, iva FROM comprobante WHERE pagado = 1 AND id_user = ?";
        $result = $this->db->query($sql, array($id));
        $row = $result->result();
        return $row;
    }

    public function calcularImpuestos($total) {
        $sql = "SELECT limite_inferior as inferior, limite_superior as superior, cuota_fija as cuota, porcentaje FROM datos_calculo";
        $result = $this->db->query($sql);
        $row = $result->result();
        foreach($row as $calculo) {
            if(($calculo->inferior < $total) && ($total < $calculo->superior)){
                // Utilidades del mes
                echo("<td>$".number_format($total,2,".",",")."</td>");

                // Calculo Inferior
                echo("<td>$".number_format($calculo->inferior,2,".",",")."</td>");

                // Total menos limite inferior
                $total_final = $total - $calculo->inferior;
                echo("<td>$".number_format($total_final,2,".",",")."</td>");

                // Tasa ISR
                echo("<td>".number_format($calculo->porcentaje,2,".",",")."%</td>");

                // ISR
                $total_final = ($total_final * $calculo->porcentaje) / 100;
                echo("<td>$".number_format($total_final,2,".",",")."</td>");

                // Cuota Fija
                echo("<td>$".number_format($calculo->cuota,2,".",",")."</td>");

                // Impuesto Determinado
                $total_final += $calculo->cuota;
                echo("<td>$".number_format($total_final,2,".",",")."</td>");

            }
        }
    }

    public function getComprobante($id) {
        $sql = "SELECT * FROM comprobante WHERE id_factura = ?";
        $result = $this->db->query($sql, array($id));
        $row = $result->row();
        return $row;
    }

    public function getEmisor($comprobante) {
        $sql = "SELECT * FROM emisor WHERE no_comprobante = ?";
        $result = $this->db->query($sql, array($comprobante));
        $row = $result->row();
        return $row;
    }

    public function insertarComprobante($comprobante) {

        $datosComprobante = array(
            'id_user' => $comprobante['id_user'],
            'no_comprobante' => $comprobante['no_comprobante'],
            'fecha' => $comprobante['fecha'],
            'mes' => $comprobante['mes'],
            'ano' => $comprobante['ano'],
            'subtotal' => $comprobante['subtotal'],
            'moneda' => $comprobante['moneda'],
            'total' => $comprobante['total'],
            'tipo_comprobante' => $comprobante['tipo_comprobante'],
            'metodo_pago' => $comprobante['metodo_pago'],
            'iva' => $comprobante['iva'],
            'tasa_iva' => $comprobante['tasa_iva'],
            'archivo' => $comprobante['archivo'],

            'rfc_cliente' => $comprobante['rfc_cliente'],
            'nombre_cliente' => $comprobante['nombre_cliente'],
            'calle_cliente' => $comprobante['calle_cliente'],
            'no_exterior_cliente' => $comprobante['no_exterior_cliente'],
            'colonia_cliente' => $comprobante['colonia_cliente'],
            'municipio_cliente' => $comprobante['municipio_cliente'],
            'estado_cliente' => $comprobante['estado_cliente'],
            'pais_cliente' => $comprobante['pais_cliente'],
            'cp_cliente' => $comprobante['cp_cliente'],
        );

        $query = $this->db->insert('comprobante',$datosComprobante);

        if($query) {
            return true;
        }
        else {
            return false;
        }
    }

    public function tomarRFC($id) {
        $sql = "SELECT rfc FROM datos WHERE id_user = ?";
        $result = $this->db->query($sql, array($id));
        $row = $result->row();
        return $row->rfc;
    }

    public function totalDeuda($rfc, $tipo_comprobante) {
        $total = 0;
        $sql = "SELECT subtotal FROM comprobante WHERE rfc_cliente = ? AND tipo_comprobante = ?";
        $result = $this->db->query($sql, array($rfc, $tipo_comprobante));
        $row = $result->result();
//        echo("<pre>");
//        print_r($row);
//        echo("</pre>");
        foreach($row as $fila) {
            $total += $fila->subtotal;
        }
        return $total;
    }

}