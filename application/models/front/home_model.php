<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insertar_usuario($id) {
        $data = array(
            'id_user' => $id,
            'nombre_completo' => $this->input->post('nombre_txt'),
            'rfc' => strtoupper($this->input->post('rfc_txt')),
        );

        $query = $this->db->insert('datos',$data);

        if($query) {
            $datos_pago = array(
                'id_user' => $id,
                'fecha_pago' => '',
                'fecha_renovar_pago' => '',
            );
            $fecha = date_create();
            $datos_pago['fecha_pago'] = date_format($fecha, 'Y-m-d');
            $fecha_final = date_add($fecha, date_interval_create_from_date_string('30 days'));
            $datos_pago['fecha_renovar_pago'] = date_format($fecha_final, 'Y-m-d');

            $query2 = $this->db->insert('pago',$datos_pago);

            if($query2) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    public function actualiarDatosUsuario($datos) {

        foreach($datos as $key => $value ) {
            $data[$key] = $value;
        }

        $this->db->where('id_user', $this->tank_auth->get_user_id());
        $query = $this->db->update('datos', $data);

        if($query) {
            return true;
        }
        else {
            return false;
        }
    }
}