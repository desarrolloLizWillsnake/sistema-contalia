<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="icon" href="<?= base_url('img/favicon.ico') ?>">

    <title>Modificar Contraseña | CONTALIA</title>

    <link href="<?= base_url('css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/login.css') ?>" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<!--Change Password-->
    <div class="form-signin-logo">
        <img src="<?= base_url('img/contalia.png') ?>">
    </div>

<div class="container-login">
<?php
$old_password = array(
	'name'	=> 'old_password',
	'id'	=> 'old_password',
	'value' => set_value('old_password'),
	'size' 	=> 30,
	'placeholder' => "Contraseña Anterior",
	'class' => "form-control",
);
$new_password = array(
	'name'	=> 'new_password',
	'id'	=> 'new_password',
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
	'placeholder' => "Nueva Contraseña",
	'class' => "form-control",
);
$confirm_new_password = array(
	'name'	=> 'confirm_new_password',
	'id'	=> 'confirm_new_password',
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size' 	=> 30,
	'placeholder' => "Confirmar Nueva Contraseña",
	'class' => "form-control",
);
?>
<?php echo form_open($this->uri->uri_string()); ?>

<div class="form-signin form-signin2">

	<h3 class="center">MODIFICAR CONTRASEÑA</h3>

	<!--Error Box-->
	<div class="error_box">
		<?php echo form_error($old_password['name']); ?><?php echo isset($errors[$old_password['name']])?$errors[$old_password['name']]:''; ?>
		<?php echo form_error($new_password['name']); ?><?php echo isset($errors[$new_password['name']])?$errors[$new_password['name']]:''; ?>
		<?php echo form_error($confirm_new_password['name']); ?><?php echo isset($errors[$confirm_new_password['name']])?$errors[$confirm_new_password['name']]:''; ?>
	</div>
	
	<div class="input-group">
        <!--<div class="form-label"><?php echo form_label('Contraseña Anterior', $old_password['id']); ?></div>-->
        <span class="input-group-addon"><span class="glyphicon glyphicon-lock c-icon"></span></span>
        <?php echo form_password($old_password); ?>
    </div> 
    <div class="input-group">
        <!--<div class="form-label"><?php echo form_label('Nueva Contraseña', $new_password['id']); ?></div>-->
        <span class="input-group-addon"><span class="glyphicon glyphicon-lock c-icon"></span></span>
        <?php echo form_password($new_password); ?>
    </div> 
    <div class="input-group">
        <!--<div class="form-label"><?php echo form_label('Confirmar Nueva Contraseña', $confirm_new_password['id']); ?></div>-->
        <span class="input-group-addon"><span class="glyphicon glyphicon-lock c-icon"></span></span>
        <?php echo form_password($confirm_new_password); ?>
    </div>	
		
		<?php echo form_submit('change', 'CAMBIAR CONTRASEÑA'); ?>
		<?php echo form_close(); ?>
</div>
</div><!-- /container -->

</body>
</html>