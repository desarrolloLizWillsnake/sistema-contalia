<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login | CONTALIA</title>

    <link href="<?= base_url('css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/login.css') ?>" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!--Login-->
    <div class="form-signin-logo">
        <img src="<?= base_url('img/contalia.png') ?>">
    </div>
<div class="container-login">
    <?php
    $login = array(
        'name'	=> 'login',
        'id'	=> 'login',
        'value' => set_value('login'),
        'maxlength'	=> 80,
        'size'	=> 30,
        'class' => "form-control",
        'required' => 'required',
        'placeholder' => 'Correo Electrónico',
        'autofocus' => 'autofocus'

    );
    if ($login_by_username AND $login_by_email) {
        $login_label = 'Email o usuario';
    } else if ($login_by_username) {
        $login_label = 'Login';
    } else {
        $login_label = 'Correo Electrónico';
    }
    $password = array(
        'name'	=> 'password',
        'id'	=> 'password',
        'size'	=> 30,
        'class' => "form-control",
        'placeholder' => 'Contraseña',
        'required' => 'required'
    );
    $remember = array(
        'name'	=> 'remember',
        'id'	=> 'remember',
        'value'	=> 1,
        'checked'	=> set_value('remember'),
        'style' => 'margin:0;padding:0',
    );
    $captcha = array(
        'name'	=> 'captcha',
        'id'	=> 'captcha',
        'maxlength'	=> 8,
    );

    $atributos = array('class' => 'form-signin', 'role' => 'form');
    $submit_atributos= array('class' => 'btn btn-lg btn-primary btn-block', 'name' => 'submit');

    ?>

    

    <?php echo form_open($this->uri->uri_string(), $atributos); ?>
    
    <h2 class="center">INICIAR SESIÓN</h2>
        <!--Error Box-->
        <div class="error_box">
            <?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>
            <?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?>
            <?php echo form_error('recaptcha_response_field'); ?>
            <?php echo form_error($captcha['name']); ?>
        </div>

        <!--<div class="form-label"><?php echo form_label($login_label, $login['id']); ?></div>-->
        <div class="input-group">
            <span class="input-group-addon"><img src="<?= base_url('img/icono-user.png') ?>"></span>
            <?php echo form_input($login); ?>
        </div>
        <!--<td style="color: red;"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?></td>-->
        <!--<div class="form-label"><?php echo form_label('Contraseña', $password['id']); ?></div>-->
        <div class="input-group">
            <span class="input-group-addon"><img src="<?= base_url('img/icono-lock.png') ?>"></span>
            <?php echo form_password($password); ?>
        </div>
        <!--<td style="color: red;"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?></td>-->

        <?php if ($show_captcha) {
            if ($use_recaptcha) { ?>
                <div id="recaptcha_image" ></div>
            
                <a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a>
                <div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
                <div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>
                
                <div class="recaptcha_only_if_image">Introduzca las letras</div>
                <div class="recaptcha_only_if_audio">Introduzca los numeros que escuche</div>
                
                <input type="text" id="recaptcha_response_field" name="recaptcha_response_field" />
                <!--<td style="color: red;"><?php echo form_error('recaptcha_response_field'); ?></td>-->

                
                    <?php echo $recaptcha_html; ?>
    
            <?php } else { ?>
                <div class="content-recaptcha" >
                    <!--<p>Introduzca el código exactamente como aparece:</p>-->
                    <span class="content-recaptcha-span1"><?php echo $captcha_html; ?></span>
                    <!--<?php echo form_label('Código de confirmación', $captcha['id']); ?>-->
                   <span class="content-recaptcha-span2"><?php echo form_input($captcha); ?></span>
                   <!--<td style="color: red;"><?php echo form_error($captcha['name']); ?>-->
                </div>
            <?php }
        } ?>
                <div class="content-remember">
                    <?php echo form_checkbox($remember); ?>
                    <?php echo form_label('MANTENER SESIÓN', $remember['id']); ?>
                </div>

                <?php echo form_submit($submit_atributos, 'ENTRAR'); ?>
                
                <div class="content-options">
                    <span class="btn-options" style="float:left;"><?php echo anchor('/auth/forgot_password/', '¿OLVIDÓ SU CONTRASEÑA?'); ?></span>
                    <span class="btn-options" style="float:right; color: #C0D72E !important;"><?php if ($this->config->item('allow_registration', 'tank_auth')) echo anchor('/auth/register/', 'REGISTRARSE'); ?></span>
                </div>
    
    <?php echo form_close(); ?>
</div> <!--End container-login-->
    
 
    <!--<div class="container-login-extra">
        <span class="info" >CONTALIA es un grupo de contandores comprometidos con el desarrollo de las personas y sus negocios, brindando siempre soluciones  basadas 
        en nuestro compromiso por la calidad.</span>
        <a href="http://www.contalia.com.mx/" target="_self">
            <span class="button">
                <button type="button" class="btn btn-success">CONÓCENOS</button>
            </span>
        </a>
    </div>-->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
</body>
</html>