<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url('img/favicon.ico') ?>">

    <title>Registro | CONTALIA</title>

    <link href="<?= base_url('css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/login.css') ?>" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<style type="text/css">
    .form-signin {
        padding-bottom: 3%;
    }
</style>

<body>
    <!--Register-->
    <div class="form-signin-logo">
        <img src="<?= base_url('img/contalia.png') ?>">
    </div>
<div class="container-login">
    <?php
    if ($use_username) {
    	$username = array(
    		'name'	=> 'username',
    		'id'	=> 'username',
    		'value' => set_value('username'),
    		'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
    		'size'	=> 30,
            'class' => "form-control",
            'style' => "margin-bottom: 3%;",
            'placeholder' => "Usuario",
    	);
    }
    $email = array(
    	'name'	=> 'email',
    	'id'	=> 'email',
    	'value'	=> set_value('email'),
    	'maxlength'	=> 80,
    	'size'	=> 30,
        'class' => "form-control",
        'placeholder' => "Correo Electrónico",
    );
    $password = array(
    	'name'	=> 'password',
    	'id'	=> 'password',
    	'value' => set_value('password'),
    	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
    	'size'	=> 30,
        'class' => "form-control",
        'placeholder' => "Contraseña",
    );
    $confirm_password = array(
    	'name'	=> 'confirm_password',
    	'id'	=> 'confirm_password',
    	'value' => set_value('confirm_password'),
    	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
    	'size'	=> 30,
        'class' => "form-control",
        'placeholder' => "Confirmar Contraseña",
    );
    $captcha = array(
    	'name'	=> 'captcha',
    	'id'	=> 'captcha',
    	'maxlength'	=> 8,
        'class' => "form-control",
        'placeholder' => 'Código de confirmación',
    );

    $atributos = array('class' => 'form-signin', 'role' => 'form');
    $submit_atributos= array('class' => 'btn btn-lg btn-primary btn-block', 'name' => 'register');

    ?>
    <?php echo form_open($this->uri->uri_string(), $atributos); ?>
    
        <h2 class="center">REGISTRO</h2>

        <!--Error Box-->
        <div class="error_box">
            <?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?>
            <?php echo form_error($password['name']); ?>
            <?php echo form_error($confirm_password['name']); ?>
            <?php echo form_error('recaptcha_response_field'); ?>
            <?php echo form_error($captcha['name']); ?>
        </div>

        

            <?php if ($use_username) { ?>
                <div class="input-group">
                    <!--<div class="form-label"><?php echo form_label('Usuario', $username['id']); ?></div>-->
                    <span class="input-group-addon"><img src="<?= base_url('img/icono-user.png') ?>"></span>
                    <?php echo form_input($username); ?>
                    <div class="error_box"><?php echo form_error($username['name']); ?><?php echo isset($errors[$username['name']])?$errors[$username['name']]:''; ?></div>
                </div>   
            <?php } ?>

            <div class="input-group">
                <!--<div class="form-label"><?php echo form_label('Correo Electrónico', $email['id']); ?></div>-->
                <span class="input-group-addon"><img src="<?= base_url('img/icono-user.png') ?>"></span>
                <?php echo form_input($email); ?>
            </div>

            <div class="input-group">
                <!--<div class="form-label"><?php echo form_label('Contraseña', $password['id']); ?></div>-->
                <span class="input-group-addon"><img src="<?= base_url('img/icono-lock.png') ?>"></span>
                <?php echo form_password($password); ?>
            </div>

            <div class="input-group">
                <!--<div class="form-label"><?php echo form_label('Confirmar Contraseña', $confirm_password['id']); ?></div>-->
                <span class="input-group-addon"><img src="<?= base_url('img/icono-lock.png') ?>"></span>
                 <?php echo form_password($confirm_password); ?>
            </div>


            <?php if ($captcha_registration) {
                if ($use_recaptcha) { ?>
                
                    <div id="recaptcha_image"></div>
            
                    <a href="javascript:Recaptcha.reload()">Refresacar CAPTCHA</a>
                    <div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Audio CAPTCHA</a></div>
                    <div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">CAPTCHA de imagen</a></div>
            
                    <div class="recaptcha_only_if_image">Ingrese las palabras arriba</div>
                    <div class="recaptcha_only_if_audio">Enter the numbers you hear</div>
        
                <input type="text" id="recaptcha_response_field" name="recaptcha_response_field" />
               
                <?php echo $recaptcha_html; ?>
        
            <?php } else { ?>
                 <div class="content-recaptcha">
                    <div class="input-group" style="width:100%; margin-bottom: 10%;">
                        <span class="input-group-addon c-icon"><span class="glyphicon glyphicon-qrcode"></span></span>
                        <!--<?php echo form_label('Código de confirmación', $captcha['id']); ?>-->
                        <?php echo form_input($captcha); ?>
                    </div>
                    <!--<p>Ingrese las letras y números de la imagen:</p>-->
                     <span class="img-recaptcha"><?php echo $captcha_html; ?></span>
                </div>
            <?php }
            } ?>
                
               
        <?php echo form_submit($submit_atributos, 'REGISTRAR'); ?>
        <?php echo form_close(); ?>
        </div>
</div> <!-- /End container-login -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
</body>
</html>
