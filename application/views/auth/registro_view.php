<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url('img/favicon.ico') ?>">

    <title>Finalizar Registro</title>

    <link href="<?= base_url('css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/login.css') ?>" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<?php
$nombre = array(
    'name'        => 'nombre_txt',
    'id'          => 'nombre',
    'placeholder'   => 'Su nombre completo',
    'class'       => 'form-control',
    'required'  => 'required',
    'value' => set_value('nombre_txt'),
);

$rfc = array(
    'name'        => 'rfc_txt',
    'id'          => 'rfc',
    'placeholder'   => 'Su RFC',
    'class'       => 'form-control',
    'required'  => 'required',
    'value' => set_value('rfc_txt'),
);

?>

    <!--Login-->
    <div class="form-signin-logo">
        <img src="<?= base_url('img/contalia.png') ?>">
    </div>

    <div class="cont-user-data">
        <?php echo form_open('auth/terminar_registro', array("role" => "form"));?>
        <?php echo form_hidden('user_id', $user_id); ?>
        <?php echo form_hidden('new_email_key', $new_email_key); ?>
            
            <div class="col-lg-12 user-data">
                <div class="well well-sm"><strong><i class="glyphicon glyphicon-ok form-control-feedback"></i> Campos obligatorios</strong></div>
                <div class="form-group">
                    <?php echo form_label('Nombre de la persona física o persona moral', 'nombre'); ?>
                    <?php echo form_error('nombre_txt'); ?>
                    <div class="input-group">
                        <?php echo form_input($nombre); ?>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo form_label('RFC', 'rfc'); ?>
                    <?php echo form_error('rfc_txt'); ?>
                    <div class="input-group">
                        <?php echo form_input($rfc); ?>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                    </div>
                </div>
                <?php echo form_submit(array("id" => "submit", "class" => "btn btn-success", "name" => "submit"), 'Registrar'); ?>
            </div>
        <?php echo form_close(); ?>
    </div>
    
   

</div>

<script src="<?= base_url('js/jquery.min.js') ?>"></script>
<script src="<?= base_url('js/bootstrap.min.js') ?>"></script>
</body>
</html>