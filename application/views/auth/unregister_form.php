<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="icon" href="<?= base_url('img/favicon.ico') ?>">

    <title>Anular Registro | CONTALIA</title>

    <link href="<?= base_url('css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/login.css') ?>" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<!--Forgot Password-->
    <div class="form-signin-logo">
        <img src="<?= base_url('img/contalia.png') ?>">
    </div>

<div class="container-login">
<?php
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'size'	=> 30,
	'placeholder' => "Contraseña",
	'class' => "form-control",
);
?>
<?php echo form_open($this->uri->uri_string()); ?>
<div class="form-signin form-signin2">

	<h3 class="center">ANULAR CONTRASEÑA</h3>
	
	<!--Error Box-->
	<div class="error_box">
	    <?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?>
	</div>

	<div class="input-group">
        <!--<div class="form-label"><?php echo form_label('Contraseña', $password['id']); ?><div>-->
        <span class="input-group-addon"><span class="glyphicon glyphicon-lock c-icon"></span></span>
        <?php echo form_password($password); ?></td>
    </div> 

		<?php echo form_submit('cancel', 'ELIMINAR CUENTA'); ?>
		<?php echo form_close(); ?>
	</div>
</div>
</div>

</body>
</html>