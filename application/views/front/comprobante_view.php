<?php
$id_comprobante = $this->uri->segment(3);
$comprobante = $this->factura_model->getComprobante($id_comprobante);

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Revisar Comprobante</title>

    <link href="<?= base_url('css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/dashboard.css') ?>" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= base_url('dashboard') ?>"><?= $usuario ?></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <li class="active"><a href="<?= base_url('dashboard/comprobantes/'.date("F")) ?>">Comprobantes</a></li>
                <li><a href="<?= base_url('dashboard/contacto') ?>">Contacto</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?= base_url('auth/logout') ?>">Salir</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-md-12 main">
            <h2 class="sub-header">Datos del Receptor</h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>No. de Comprobante</th>
                            <th>Fecha</th>
                            <th>Subtotal</th>
                            <th>Moneda</th>
                            <th>Total</th>
                            <th>Tipo de Comprobante</th>
                            <th>Metodo de Pago</th>
                            <th>Pagado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            echo("<td>".$comprobante->no_comprobante."</td>");
                            echo("<td>".$comprobante->fecha."</td>");
                            echo("<td>$".number_format($comprobante->subtotal,2,".",",")."</td>");
                            echo("<td>".$comprobante->moneda."</td>");
                            echo("<td>$".number_format($comprobante->total,2,".",",")."</td>");
                            echo("<td>".$comprobante->tipo_comprobante."</td>");
                            echo("<td>".$comprobante->metodo_pago."</td>");
                            if($comprobante->pagado == 0){
                                echo("<td>No</td>");
                            }
                            else {
                                echo("<td>Si</td>");
                            }
                            ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 main">
            <h2 class="sub-header">Datos del Emisor</h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>No. de Comprobante</th>
                        <th>RFC del emisor</th>
                        <th>Nombre</th>
                        <th>Calle</th>
                        <th>Número Exterior</th>
                        <th>Colonia</th>
                        <th>Municipio</th>
                        <th>Estado</th>
                        <th>País</th>
                        <th>Código Postal</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <?php
                        echo("<td>".$comprobante->no_comprobante."</td>");
                        echo("<td>".$comprobante->rfc_cliente."</td>");
                        echo("<td>".$comprobante->nombre_cliente."</td>");
                        echo("<td>".$comprobante->calle_cliente."</td>");
                        echo("<td>".$comprobante->no_exterior_cliente."</td>");
                        echo("<td>".$comprobante->colonia_cliente."</td>");
                        echo("<td>".$comprobante->municipio_cliente."</td>");
                        echo("<td>".$comprobante->estado_cliente."</td>");
                        echo("<td>".$comprobante->pais_cliente."</td>");
                        echo("<td>".$comprobante->cp_cliente."</td>");
                        ?>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>

<script src="<?= base_url('js/jquery.min.js') ?>"></script>
<script src="<?= base_url('js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('js/docs.min.js') ?>"></script>
<script src="<?= base_url('js/efectos.js') ?>"></script>
</body>
</html>