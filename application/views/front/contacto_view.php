<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url('img/favicon.ico') ?>">

    <title>Contáctanos</title>

    <link href="<?= base_url('css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/dashboard.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/login.css') ?>" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="logo center">
        <img src="<?= base_url('img/contalia.png') ?>">
    </div>
    <div class="container-fluid menu">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand user" style="color:#FFF;" href="<?= base_url('dashboard') ?>"><span class="glyphicon glyphicon-user"></span> <?= $usuario ?></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left cont-menu">
                <li><a href="<?= base_url('dashboard/comprobantes/'.date("F")) ?>">Comprobantes</a></li>
                <li class="active"><a href="<?= base_url('dashboard/contacto') ?>">Contacto</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?= base_url('auth/logout') ?>"><span class="glyphicon glyphicon-log-in icon color"></span></a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container-fluid cont_principal">
    <div class="row">
        <div class="col-sm-12 col-md-12 main">
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2" style="padding-top: 30%;">
                <li role="presentation" class="dropdown-header">Dropdown header</li>
                <li>Algo</li>
                <li>Algo</li>
                <li>Algo</li>
                <li>Algo</li>
                <li role="presentation" class="divider"></li>
                <li role="presentation" class="dropdown-header">Dropdown header</li>
                ...
            </ul>
        </div>
    </div>
</div>

<script src="<?= base_url('js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('js/docs.min.js') ?>"></script>
<script src="<?= base_url('js/jquery.min.js') ?>"></script>
</body>
</html>