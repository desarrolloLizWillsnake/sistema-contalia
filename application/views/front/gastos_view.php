<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url('img/favicon.ico') ?>">

    <title>Ingresos de <?= $usuario ?></title>

    <link href="<?= base_url('css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/dashboard.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/login.css') ?>" rel="stylesheet">

    <?php
    foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="logo center">
        <img src="<?= base_url('img/contalia.png') ?>">
    </div>
    <div class="container-fluid menu">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand user" style="color:#FFF;" href="<?= base_url('dashboard') ?>"><span class="glyphicon glyphicon-user"></span> <?= $usuario ?></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left cont-menu">
                <li><a href="<?= base_url('dashboard/comprobantes/'.date("F")) ?>">Comprobantes</a></li>
                <li><a href="<?= base_url('dashboard/contacto') ?>">Contacto</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?= base_url('auth/logout') ?>"><span class="glyphicon glyphicon-log-in icon color"></span></a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container-fluid cont_principal">
    <div class="row">
        <div class="col-sm-12 col-md-12 main">
            <h2 class="sub-header title2 center">Gastos</h2>
            <h4 class="sub-header subtitle"><span class="subtitle-s">Agrega</span> <span class="separator">|</span> Para añadir los gastos sube los archivos correspodientes.</h4>
            <div class="cont-load">
                <form class="load-file" action="<?= base_url("dashboard/subir_archivo/"."gastos") ?>" method="POST" id="upload_file" enctype="multipart/form-data">
                    <input type="file" name="files[]" multiple />
                    <?php echo form_submit('subirArchivo', 'Subir Archivo'); ?>
                <?php echo form_close(); ?>
            </div>
            <h4 class="sub-header subtitle"><span class="subtitle-s">Consulta</span> <span class="separator">|</span> Selecciona el mes correspondiente a tus gastos.</h4>

            <?php echo $error;?>
            <?= form_open("dashboard/ingresos", array("id" => "formMes")) ?>
            <div class="btn-group btn-group-lg">
                <a id="mesJanuary" href="<?= base_url("dashboard/gastos/January") ?>" class="btn btn-default btn-month">Enero</a>
                <a id="mesFebruary" href="<?= base_url("dashboard/gastos/February") ?>" class="btn btn-default btn-month">Febrero</a>
                <a id="mesMarch" href="<?= base_url("dashboard/gastos/March") ?>" class="btn btn-default btn-month">Marzo</a>
                <a id="mesApril" href="<?= base_url("dashboard/gastos/April") ?>" class="btn btn-default btn-month">Abril</a>
                <a id="mesMay" href="<?= base_url("dashboard/gastos/May") ?>" class="btn btn-default btn-month">Mayo</a>
                <a id="mesJune" href="<?= base_url("dashboard/gastos/June") ?>" class="btn btn-default btn-month">Junio</a>
                <a id="mesJuly" href="<?= base_url("dashboard/gastos/July") ?>" class="btn btn-default btn-month">Julio</a>
                <a id="mesAugust" href="<?= base_url("dashboard/gastos/August") ?>" class="btn btn-default btn-month">Agosto</a>
                <a id="mesSeptember" href="<?= base_url("dashboard/gastos/September") ?>" class="btn btn-default btn-month2">Septiembre</a>
                <a id="mesOctober" href="<?= base_url("dashboard/gastos/October") ?>" class="btn btn-default btn-month">Octubre</a>
                <a id="mesNovember" href="<?= base_url("dashboard/gastos/November") ?>" class="btn btn-default btn-month2">Noviembre</a>
                <a id="mesDecember" href="<?= base_url("dashboard/gastos/December") ?>" class="btn btn-default btn-month2">Diciembre</a>
            </div>
            <?= form_close() ?>
            <?php echo $output; ?>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="--><?//= base_url('js/jquery.min.js') ?><!--"></script>-->
<script>
    var mes = "#mes"+"<?php echo $mesactivo ?>";
    $( mes ).toggleClass( "active" );
</script>

<script src="<?= base_url('js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('js/docs.min.js') ?>"></script>
<script src="<?= base_url('js/efectos.js') ?>"></script>
</body>
</html>