<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url('img/favicon.ico') ?>">

    <title>Historial | <?= $usuario ?></title>

    <link href="<?= base_url('css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/dashboard.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/login.css') ?>" rel="stylesheet">

    <?php
    foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body  class="page-historial">
<script>
    $(function () {
        var ingresos = "<?php echo $ingresos ?>" ;
        var egresos = "<?php echo $egresos ?>" ;
        $('#container').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'CALCULO DEL RANGO'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    shadow: false,
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                type: 'pie',
                innerSize: '75%',
                size: '100%',
                showInLegend:true,
                data: [
                    ['Ingresos',  parseFloat(ingresos)],
                    ['Egresos',   parseFloat(egresos)]
                ]
            }]
        });
    });
</script>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid menu">
        <div class="logo">
            <a href="<?= base_url('dashboard') ?>"><img src="<?= base_url('img/contalia.png') ?>"></a>
        </div>

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav cont-menu">
                <li><a href="<?= base_url('dashboard/tu_perfil') ?>">TU PERFIL</a></li>
                <li><a href="<?= base_url('dashboard/comprobantes/'.date("F")) ?>">MOVIMIENTOS</a></li>
                <li class="active"><a href="<?= base_url('dashboard/historial') ?>">HISTORIAL</a></li>
                <li><a href="#">SOPORTE</a></li>
                <li><a href="<?= base_url('auth/logout') ?>">CERRAR SESIÓN</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="cont-ini-gral">
    <div class="info-user">
        <span class="span1">Usuario: <a class="user" href="<?= base_url('dashboard') ?>"> <?= $usuario ?></a></span>
        <span class="span2"><?= $fecha ?></span>
    </div>
</div>



<div class="container-fluid cont_principal">
    <div class="row">
        <div class="title center"> <h3>HISTORIAL</h3> </div>
        <div class="col-sm-12 col-md-12 cont-historial main">
            <!-- Selección de rango para calcular sus cuentas -->
            <div class="cont-search center">
                <form role="form" action="<?= base_url("dashboard/historial_mes") ?>" method="POST">
                    <div class="form-group">
                        <h4>HISTORIAL POR MES</h4>
                        <select name="mes" class="btn btn-default" required>
                            <option value="">SELECCIONAR MES PARA VER CUENTAS</option>
                            <option value="01">ENERO</option>
                            <option value="02">FEBRERO</option>
                            <option value="03">MARZO</option>
                            <option value="04">ABRIL</option>
                            <option value="05">MAYO</option>
                            <option value="06">JUNIO</option>
                            <option value="07">JULIO</option>
                            <option value="08">AGOSTO</option>
                            <option value="09">SEPTIEMBRE</option>
                            <option value="10">OCTUBRE</option>
                            <option value="11">NOVIEMBRE</option>
                            <option value="12">DICIEMBRE</option>
                        </select>
                        <select name="year" class="btn btn-default">
                            <option value="">AÑO</option>
                            <?php
                                foreach($years as $ano) {
                                    echo("<option value=".$ano->ano.">".$ano->ano."</option>");
                                }
                            ?>
                        </select>
                        <button type="submit" class="btn btn-default">CONSULTAR</button>
                    </div>    
                </form>
                <form role="form" action="<?= base_url("dashboard/historial_rango") ?>" method="POST">
                    <div class="form-group">
                        <h4>RANGO DEL HISTORIAL POR MESES</h4>
                        <select name="mes_inicio" class="btn btn-default" required>
                            <option value="">DEL MES</option>
                            <option value="01">ENERO</option>
                            <option value="02">FEBRERO</option>
                            <option value="03">MARZO</option>
                            <option value="04">ABRIL</option>
                            <option value="05">MAYO</option>
                            <option value="06">JUNIO</option>
                            <option value="07">JULIO</option>
                            <option value="08">AGOSTO</option>
                            <option value="09">SEPTIEMBRE</option>
                            <option value="10">OCTUBRE</option>
                            <option value="11">NOVIEMBRE</option>
                            <option value="12">DICIEMBRE</option>
                        </select>
                        <select name="mes_fin" class="btn btn-default" required>
                            <option value="">AL MES</option>
                            <option value="01">ENERO</option>
                            <option value="02">FEBRERO</option>
                            <option value="03">MARZO</option>
                            <option value="04">ABRIL</option>
                            <option value="05">MAYO</option>
                            <option value="06">JUNIO</option>
                            <option value="07">JULIO</option>
                            <option value="08">AGOSTO</option>
                            <option value="09">SEPTIEMBRE</option>
                            <option value="10">OCTUBRE</option>
                            <option value="11">NOVIEMBRE</option>
                            <option value="12">DICIEMBRE</option>
                        </select>
                        <select name="year" class="btn btn-default">
                            <option value="">AÑO</option>
                            <?php
                                foreach($years as $ano) {
                                    echo("<option value=".$ano->ano.">".$ano->ano."</option>");
                                }
                            ?>
                        </select>
                        <button type="submit" class="btn btn-default">CONSULTAR</button>
                    </div>
                </form>
            </div>
            <div class="form-load third center">
                <P><?php if(isset($rango)) echo($rango); ?></P>
            </div>
            <div class="container-fluid cont_principal" style="background:#FFF; padding:0; border-bottom: 1.5em solid #707070;">  
                <?php echo $output; ?>
            </div> 
           
        </div>
    </div>

    <h3 class="subtitle center">BALANCE DENTRO DEL RANGO</h3>
    <div class="info-historial center">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="info-title-historial"> INGRESOS </div> 
                <div class="info-money-historial">
                    <h3>$<?= number_format($ingresos ,2,".",",") ?></h3>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="info-title-historial"> GASTOS </div> 
                <div class="info-money-historial">
                    <h3>$<?= number_format($egresos ,2,".",",") ?></h3>
                </div> 
            </div>
        </div>
    </div>
</div>

<!--    Gráfica-->
<div class="cont-ini-gral-lower historial-lower center">
    <div class="graph-2">
        <div id="container"></div>
    </div>
</div>
<!-- /grafica-->
    

<div class="row cont-footer">
    <div class="col-md-5 footer1">
        <img src="<?= base_url('img/logo-contalia.png') ?>">
        <p><span class="glyphicon glyphicon-phone-alt"></span> 6305 6496
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="glyphicon glyphicon-envelope"></span> contacto@contalia.com.mx
        </p>
    </div>
    <div class="col-md-3 footer2">
        <h4>SISTEMA</h4>
        <div class="row">
            <div class="col-md-6">
                <p>> COMO FUNCIONA</p>
                <p>> PAQUETES</p>
            </div>
            <div class="col-md-6">
                <p>> FAQ</p>
                <p>> SOPORTE</p>
            </div>
        </div>
    </div>
    <div class="col-md-4 footer3 center">
        <img src="<?= base_url('img/icono-facebook.png') ?>">
        <img src="<?= base_url('img/icono-twitter.png') ?>">
        <img src="<?= base_url('img/icono-linkedin.png') ?>">
        <img src="<?= base_url('img/icono-google.png') ?>">
    </div>
</div>
<div class="footer">CONTALIA 2014. TODOS LOS DERECHOS RESERVADOS</div>

</div>

<script>
    $('.gasto').closest('tr').css('background-color','#FFC3C0');
    $('.ingreso').closest('tr').css('background-color','#CCFFCC');
</script>

<script src="<?= base_url('js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('js/docs.min.js') ?>"></script>
<script src="<?= base_url('js/efectos.js') ?>"></script>
<script src="<?= base_url('js/highcharts.js') ?>"></script>
<script src="<?= base_url('js/modules/exporting.js') ?>"></script>
</body>
</html>