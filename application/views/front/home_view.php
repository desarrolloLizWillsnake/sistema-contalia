<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url('img/favicon.ico') ?>">

    <title>Bienvenido <?= $usuario ?></title>

    <link href="<?= base_url('css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/dashboard.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/login.css') ?>" rel="stylesheet">

    <script src="<?= base_url('js/jquery.min.js') ?>"></script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<script>
    $(function () {
        var ingresos = "<?php echo $ingresos ?>" ;
        var egresos = "<?php echo $egresos ?>" ;
        var datos = "<?= print_r($utilidades_year) ?>" ;
        datos = datos.slice(0, - 1);
        datos = datos.slice(0, - 1);
        datos = datos.slice(1);
        $('#container').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'ACTIVIDAD MENSUAL',
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#FFF',
                        connectorColor: '#FFF',
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                type: 'pie',
                name: 'Calculo del mes',
                data: [
                    ['Ingresos',  parseFloat(ingresos)],
                    ['Egresos',   parseFloat(egresos)]
                ]
            }]
        });

        $('#contenedor').highcharts({
            chart: {
                backgroundColor:'rgba(255, 255, 255, 0.1)',
                type: 'spline'
            },
            title: {
                text: 'MOVIMIENTO ANUAL',
                x: -20 //center
            },
            /*subtitle: {
                text: 'CONTALIA',
                x: -20
            },*/
            xAxis: {
                categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
            },
            yAxis: {
                title: {
                    text: 'Utilidades'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valuePrefix: '$'
            },
            lang: {
                decimalPoint: '.',
                thousandsSep: ','
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            plotOptions: {
                spline: {
                    fillOpacity: 0.5,
                    states: {
                        hover: {
                            lineWidth: 5
                        }
                    },
                    marker: {
                        enabled: false
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Utilidades',
                data: JSON.parse("[" + datos + "]")
            }]
        });

    });
</script>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid menu">
        <div class="logo">
            <a href="<?= base_url('dashboard') ?>"><img src="<?= base_url('img/contalia.png') ?>"></a>
        </div>

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
           
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav cont-menu">
                <li><a href="<?= base_url('dashboard/tu_perfil') ?>">TU PERFIL</a></li>
                <li><a href="<?= base_url('dashboard/comprobantes/'.date("F")) ?>">MOVIMIENTOS</a></li>
                <li><a href="<?= base_url('dashboard/historial') ?>">HISTORIAL</a></li>
<!--                <li><a href="--><?//= base_url('dashboard/contacto') ?><!--">SOPORTE</a></li>-->
                <li><a href="#">SOPORTE</a></li>
                <li><a href="<?= base_url('auth/logout') ?>">CERRAR SESIÓN</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="cont-ini-gral-h">
    <div class="info-user">
        <span class="span1">USUARIO: <a class="user" href="<?= base_url('dashboard') ?>"> <?= $usuario ?></a></span>
        <span class="span2">FECHA: <?= $fecha?></span>
    </div>
    <div class="graph">
        <div class="graph-mens">
            <div id="container"></div>
        </div>
        <div class="graph-anual">
            <div id="contenedor"></div>
        </div>
    </div>
</div>

<div class="container-fluid cont_principal">
    <div class="center">
        <h4>IVA POR PAGAR: Prueba</h4>
            <?php
                if($iva_por_pagar > 0.0){
                    echo('<h4 style="color: #9C3636;">$'.number_format($iva_por_pagar ,2,".",",").'<h4>');
                }
                elseif($iva_por_pagar < 0.0) {
                    echo('<h4 style="color: #FFF;">$'.number_format($iva_por_pagar ,2,".",",").'<h4>');
                }
                else{
                    echo('<h4 class="iva" style="color:#E6E6E6;">$'.number_format($iva_por_pagar ,2,".",",").'<h4>');
                }
            ?>
    </div>

    <div class="row cont-rows-home center">
        <div class="col-md-3 rows-home" style="margin-left:0;">
            <img src="<?= base_url('img/icono-ingresos.png') ?>"> <!--ingresos-->
            <div class="col-md-6">
                <span class="color-text">$<?= number_format($ingresos,2,".",",") ?></span>
            </div>
            <a class="btn btn-default button" href="<?= base_url("dashboard/comprobantes/".date("F")) ?>">INGRESOS</a>
        </div>


        <div class="col-md-3 rows-home">
            <img src="<?= base_url('img/icono-gastos.png') ?>"><!--gastos-->
            <div class="col-md-6">
                <span class="color-text">$<?= number_format($egresos,2,".",",") ?></span>
            </div>
            <a class="btn btn-default button" href="<?= base_url("dashboard/comprobantes/".date("F")) ?>">GASTOS</a>
        </div>
        <div class="col-md-3 rows-home">
            <img class="ajus-icon" src="<?= base_url('img/icono-por-cobrar.png') ?>">
            <div class="col-md-6">
                <span class="color-text">$<?= number_format($por_cobrar,2,".",",") ?></span>
            </div>
            <a class="btn btn-default button" href="<?= base_url("dashboard/cuentas_cobrar") ?>" >POR COBRAR</a>
        </div>
        <div class="col-md-3 rows-home">
            <img class="ajus-icon" src="<?= base_url('img/icono-por-pagar.png') ?>"><!--por-pagar-->
            <div class="col-md-6">
                <span class="color-text">$<?= number_format($por_pagar,2,".",",") ?></span>
            </div>
            <a class="btn btn-default button" href="<?= base_url("dashboard/cuentas_pagar") ?>">POR PAGAR</a>
        </div>
    </div>

</div>

<h3 class="sub-title center">MOVIMIENTOS</h3>

<div class="container-fluid cont_principal">
    <div class="row cont-rows-home2 center">
        <a href="<?= base_url("dashboard/comprobantes/".date("F")) ?>" style="text-decoration: none;" >
            <div class="col-md-6 rows-home2" style="margin-left: 0;">
                <img src="<?= base_url('img/icono-subir-facturas.png') ?>">
                <h4>SUBIR FACTURAS</h4>
            </div>
        </a>

        <a href="<?= base_url("dashboard/historial") ?>" style="text-decoration: none;" >
            <div class="col-md-6 rows-home2">
                <img src="<?= base_url('img/icono-historial.png') ?>">
                <h4>HISTORIAL</h4>
            </div>
        </a>
    </div>
</div>


<div class="cont-perfil-home center">
    <h3 class="center">TU PERFIL</h3>
    <div class="row cont-rows-home3">
         <!--<?//= $mes ?>-->
        <div class="col-md-3 rows-home3" style="margin-left:0;">
            <h2>43</h2>
            <h6>MOVIMIENTOS RESTANTES</h6>
            <a class="btn btn-default button" href="--><?//= base_url("dashboard/comprobantes/".date("F")) ?><!--">COMPRAR MÁS</a>
        </div>
        <div class="col-md-3 rows-home3">
            <h2>17</h2> <!--Fecha-->
        <h6>PAGO DE IMPUESTOS</h6>
            <a class="btn btn-default button" href="--><?//= base_url("dashboard/comprobantes/".date("F")) ?><!--">COMPRAR MÁS</a>
        </div>
      <div class="col-md-3 rows-home3">
            <h2>5</h2>
            <h6>PENDIENTES</h6>
            <a class="btn btn-default button" style="margin-top: 15%;" href="--><?//= base_url("dashboard/comprobantes/".date("F")) ?><!--">LEER MÁS</a>
        </div>
        <div class="col-md-3 rows-home3" >
            <h2>1</h2>
            <h6>TICKET DE SOPORTE</h6>
            <a class="btn btn-default button" style="margin-top: 15%;" href="--><?//= base_url("dashboard/comprobantes/".date("F")) ?><!--">LERR MÁS</a>
        </div>
    </div>
    <a href="#">
        <a href="<?= base_url('dashboard/tu_perfil') ?>"><img src="<?= base_url('img/icono-ajustes.png') ?>"></a>
    </a>
</div>

<div class="row cont-footer">
        <div class="col-md-5 footer1">
            <img src="<?= base_url('img/logo-contalia.png') ?>">
            <p><span class="glyphicon glyphicon-phone-alt"></span> 6305 6496 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="glyphicon glyphicon-envelope"></span> contacto@contalia.com.mx
            </p>
        </div>
        <div class="col-md-3 footer2">
            <h4>SISTEMA</h4>
            <div class="row">
                <div class="col-md-6">
                    <p>> COMO FUNCIONA</p>
                    <p>> PAQUETES</p>
                </div>
                <div class="col-md-6">
                    <p>> FAQ</p>
                    <p>> SOPORTE</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 footer3 center">
            <img src="<?= base_url('img/icono-facebook.png') ?>">
            <img src="<?= base_url('img/icono-twitter.png') ?>">
            <img src="<?= base_url('img/icono-linkedin.png') ?>">
            <img src="<?= base_url('img/icono-google.png') ?>">
        </div>
</div>
<div class="footer">CONTALIA 2014. TODOS LOS DERECHOS RESERVADOS</div>

</div>
</div>

<script src="<?= base_url('js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('js/docs.min.js') ?>"></script>
<script src="<?= base_url('js/highcharts.js') ?>"></script>
<script src="<?= base_url('js/modules/exporting.js') ?>"></script>
</body>
</html>