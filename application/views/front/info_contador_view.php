<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url('img/favicon.ico') ?>">

    <title>Cuentas por cobrar</title>

    <link href="<?= base_url('css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/dashboard.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/login.css') ?>" rel="stylesheet">

    <script src="<?= base_url('js/jquery.min.js') ?>"></script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<?php if(isset($mensaje)) echo($mensaje); ?>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid menu">
        <div class="logo">
            <a href="<?= base_url('dashboard') ?>"><img src="<?= base_url('img/contalia.png') ?>"></a>
        </div>

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav cont-menu">
                <li><a href="<?= base_url('dashboard/tu_perfil') ?>">TU PERFIL</a></li>
                <li><a href="<?= base_url('dashboard/comprobantes/'.date("F")) ?>">MOVIMIENTOS</a></li>
                <li><a href="<?= base_url('dashboard/historial') ?>">HISTORIAL</a></li>
                <li><a href="#">SOPORTE</a></li>
                <li><a href="<?= base_url('auth/logout') ?>">CERRAR SESIÓN</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="cont-ini-gral-h">
    <div class="info-user">
        <span class="span1">Usuario: <a class="user" href="<?= base_url('dashboard') ?>"> <?= $usuario ?></a></span>
        <span class="span2">Fecha: <?= $fecha?></span>
    </div>
    <div class="graph">
        <div class="graph-mens">
            <div id="container"></div>
        </div>
        <div class="graph-anual">
            <div id="contenedor"></div>
        </div>
    </div>
</div>


<div class="container-fluid cont_principal">
    <div class="row">
        <div class="col-sm-12 col-md-12 main">
            <h2 class="sub-header title2 center" style="margin-bottom: 2%;">Contador <?= $nombre_contador ?></h2>
        </div>

        <div class="col-sm-2 col-md-2">
            <a href="" data-toggle="modal" data-target="#myModal" class="btn btn-primary">Aceptarlo como mi contador</a>
        </div>

    </div>
</div>
<div class="row cont-footer">
    <div class="col-md-5 footer1">
        <img src="<?= base_url('img/logo-contalia.png') ?>">
        <p><span class="glyphicon glyphicon-phone-alt"></span> 6305 6496
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="glyphicon glyphicon-envelope"></span> contacto@contalia.com.mx
        </p>
    </div>
    <div class="col-md-3 footer2">
        <h4>SISTEMA</h4>
        <div class="row">
            <div class="col-md-6">
                <p>> COMO FUNCIONA</p>
                <p>> PAQUETES</p>
            </div>
            <div class="col-md-6">
                <p>> FAQ</p>
                <p>> SOPORTE</p>
            </div>
        </div>
    </div>
    <div class="col-md-4 footer3 center">
        <img src="<?= base_url('img/icono-facebook.png') ?>">
        <img src="<?= base_url('img/icono-twitter.png') ?>">
        <img src="<?= base_url('img/icono-linkedin.png') ?>">
        <img src="<?= base_url('img/icono-google.png') ?>">
    </div>
</div>
<div class="footer">CONTALIA 2014. TODOS LOS DERECHOS RESERVADOS</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="perfil_form" action="<?= base_url("dashboard/aceptarContador") ?>" method="POST">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">¿Desease seleccionarlo como tu contador?</h4>
                </div>
                <div class="modal-body">
                    <p>Al seleccionar a esta persona como contador, le das derecho a poder ver tu información que guardes en esta plataforma para poder auxiliarte en cualquier aspecto técnico que necesites.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Aceptar</button>
                    <input type="hidden" name="contador" value="<?= $this->encrypt->encode($contador); ?>">
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('js/docs.min.js') ?>"></script>
<script src="<?= base_url('js/efectos.js') ?>"></script>
</body>
</html>