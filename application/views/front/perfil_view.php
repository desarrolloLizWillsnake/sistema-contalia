<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url('img/favicon.ico') ?>">

    <title>Bienvenido <?= $usuario ?></title>

    <link href="<?= base_url('css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/dashboard.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/login.css') ?>" rel="stylesheet">

    <script src="<?= base_url('js/jquery.min.js') ?>"></script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="page-perfil">

<?php if(isset($mensaje)) echo($mensaje); ?>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid menu">
        <div class="logo">
            <a href="<?= base_url('dashboard') ?>"><img src="<?= base_url('img/contalia.png') ?>"></a>
        </div>

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav cont-menu">
                <li class="active"><a href="<?= base_url('dashboard/tu_perfil') ?>">TU PERFIL</a></li>
                <li><a href="<?= base_url('dashboard/comprobantes/'.date("F")) ?>">MOVIMIENTOS</a></li>
                <li><a href="<?= base_url('dashboard/historial') ?>">HISTORIAL</a></li>
                <li><a href="#">SOPORTE</a></li>
                <li><a href="<?= base_url('auth/logout') ?>">CERRAR SESIÓN</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="cont-ini-gral">
    <div class="info-user">
        <span class="span1">Usuario: <a class="user" href="<?= base_url('dashboard') ?>"> <?= $usuario ?></a></span>
        <span class="span2">Fecha: <?= $fecha?></span>
    </div>
</div>

<div class="container-fluid cont_principal">
    <div class="title center"> <h3>TU PERFIL</h3> </div>

    <div class="cont-perfil row">
        <div class="col-md-6 rows-home c-perfil-1">
            <div class="list-group">
                <a href="" class="list-group-item item-perfil" id="listoPerfil"><span class="glyphicon glyphicon-saved"></span> LISTO</a>
                <a href="" class="list-group-item item-perfil" id="planPerfil"><span class="glyphicon glyphicon-check"></span> PLAN CONTRATADO</a>
                <a href="" class="list-group-item item-perfil" id="contadorPerfil"><span class="glyphicon glyphicon-list"></span> TU CONTADOR</a>
                <a href="" class="list-group-item item-perfil" id="soportePerfil"><span class="glyphicon glyphicon-refresh"></span> SOPORTE</a>
                <a href="" data-toggle="modal" data-target="#myModal" class="list-group-item item-perfil"><span class="glyphicon glyphicon-cog"></span> EDITAR PERFIL</a>
            </div>
        </div>
        <div class="col-md-6 rows-home c-perfil-2" id="contenidoPerfil">

        </div>
    </div>

</div>
<h3 class="sub-title center">MOVIMIENTOS</h3>

<div class="container-fluid cont_principal">
    <div class="row cont-rows-home4 center">
        <a href="<?= base_url("dashboard/cuentas_cobrar")?>" style="text-decoration: none;" >
            <div class="col-md-3 rows-home5" style="margin-left: 0;">
                <img src="<?= base_url('img/icono-subir-facturas.png') ?>">
                <h4>POR COBRAR</h4>
            </div>
        </a>

        <a href="<?= base_url("dashboard/cuentas_pagar")?>" style="text-decoration: none;" >
            <div class="col-md-3 rows-home5">
                <img src="<?= base_url('img/icono-historial.png') ?>">
                <h4>POR PAGAR</h4>
            </div>
        </a>
        <a href="<?= base_url("dashboard/comprobantes/".date("F"))?>" style="text-decoration: none;" >
            <div class="col-md-3 rows-home5">
                <img src="<?= base_url('img/icono-subir-facturas.png') ?>">
                <h4>SUBIR FACTURAS</h4>
            </div>
        </a>

        <a href="<?= base_url("dashboard/historial")?>" style="text-decoration: none;" >
            <div class="col-md-3 rows-home5">
                <img src="<?= base_url('img/icono-historial.png') ?>">
                <h4>HISTORIAL</h4>
            </div>
        </a>

    </div>
</div>
<div class="cont-ini-gral-lower">
    <div class="cont-perfil-pay-tax center">
        <h3>PRÓXIMO PAGO DE IMPUESTOS</h3>
        <span><b>28</b></span>
        <h3>OCTUBRE</h3>
    </div>
</div>
<!--Footer-->
<div class="row cont-footer">
    <div class="col-md-5 footer1">
        <img src="<?= base_url('img/logo-contalia.png') ?>">
        <p><span class="glyphicon glyphicon-phone-alt"></span> 6305 6496
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="glyphicon glyphicon-envelope"></span> contacto@contalia.com.mx
        </p>
    </div>
    <div class="col-md-3 footer2">
        <h4>SISTEMA</h4>
        <div class="row">
            <div class="col-md-6">
                <p>> COMO FUNCIONA</p>
                <p>> PAQUETES</p>
            </div>
            <div class="col-md-6">
                <p>> FAQ</p>
                <p>> SOPORTE</p>
            </div>
        </div>
    </div>
    <div class="col-md-4 footer3 center">
        <img src="<?= base_url('img/icono-facebook.png') ?>">
        <img src="<?= base_url('img/icono-twitter.png') ?>">
        <img src="<?= base_url('img/icono-linkedin.png') ?>">
        <img src="<?= base_url('img/icono-google.png') ?>">
    </div>
</div>
<div class="footer">CONTALIA 2014. TODOS LOS DERECHOS RESERVADOS</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="perfil_form" action="<?= base_url("dashboard/modificar_perfil") ?>" method="POST">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="myModalLabel"> <span class="glyphicon glyphicon-edit"></span> EDITAR PERFIL</h3>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nombre_completo">NOMBRE </label>
                        <?php echo form_error('nombre_completo'); ?>
                        <input type="text" name="nombre_completo" class="form-control" id="nombre_completo" value="<?php echo set_value('username'); ?>" placeholder="Su nombre">
                    </div>
                    <div class="form-group">
                        <label for="rfc">RFC </label>
                        <?php echo form_error('rfc'); ?>
                        <input type="text" name="rfc" class="form-control" id="rfc" value="<?php echo set_value('username'); ?>" placeholder="Su RFC">
                    </div>
                    <a href="<?= base_url("auth/change_email") ?>" class="btn btn-default">CAMBIAR EMAIL</a>
                    <a href="<?= base_url("auth/change_password") ?>" class="btn btn-default">MODIFICAR CONTRASEÑA</a>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success">Guardar Cambios</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?= base_url('js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('js/docs.min.js') ?>"></script>
<script src="<?= base_url('js/efectos.js') ?>"></script>
<script>
    var modal = "<?php if(isset($error)) echo $error ?>";
    if(modal) {
        $('#myModal').modal('show');
    }
</script>
</body>
</html>