<?php
$sql = "SELECT rfc_cliente, nombre_cliente FROM comprobante WHERE tipo_comprobante = 'gasto' AND pagado = 0 GROUP BY nombre_cliente";
$result = $this->db->query($sql);
$row = $result->result();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url('img/favicon.ico') ?>">

    <title>Cuentas por pagar</title>

    <link href="<?= base_url('css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/dashboard.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/login.css') ?>" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body  class="page-accounts">
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid menu">
            <div class="logo">
                 <a href="<?= base_url('dashboard') ?>"><img src="<?= base_url('img/contalia.png') ?>"></a>
            </div>

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav cont-menu">
                    <li><a href="<?= base_url('dashboard/tu_perfil') ?>">TU PERFIL</a></li>
                    <li><a href="<?= base_url('dashboard/comprobantes/'.date("F")) ?>">MOVIMIENTOS</a></li>
                    <li><a href="<?= base_url('dashboard/historial') ?>">HISTORIAL</a></li>
                    <li><a href="#">SOPORTE</a></li>
                    <li><a href="<?= base_url('auth/logout') ?>">CERRAR SESIÓN</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="cont-ini-gral">
        <div class="info-user">
            <span class="span1">Usuario: <a class="user" href="<?= base_url('dashboard') ?>"> <?= $usuario ?></a></span>
            <span class="span2">Fecha: <!--<?= $fecha ?>--> 13 Octubre 2014</span>
        </div>
    </div>

    <div class="container-fluid cont_principal">
        <div class="title center"> <h3>CUENTAS POR PAGAR</h3> </div>
    </div>
    <div class="cont-persons">
        <div class="list-group">
                    <?php
                        foreach($row as $fila) {
                            $total = $this->factura_model->totalDeuda($fila->rfc_cliente, "gasto");
                            echo("<a href='".base_url('dashboard/datos_persona/'.$fila->rfc_cliente)."' class='list-group-item'>".
                                "<span class='s1'>".$fila->nombre_cliente."</span>".
                                "<span class='s2'>"."$ ".number_format($total,2,".",",")."<span>"."</a>");
                        }
                    ?>
        </div>
    </div>
    <div class="title center"> <h2>TOTAL A PAGAR: $0.00</h2> </div>
    <!--Footer-->
    <div class="row cont-footer">
        <div class="col-md-5 footer1">
            <img src="<?= base_url('img/logo-contalia.png') ?>">
            <p><span class="glyphicon glyphicon-phone-alt"></span> 6305 6496 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="glyphicon glyphicon-envelope"></span> contacto@contalia.com.mx
            </p>
        </div>
        <div class="col-md-3 footer2">
            <h4>SISTEMA</h4>
            <div class="row">
                <div class="col-md-6">
                    <p>> COMO FUNCIONA</p>
                    <p>> PAQUETES</p>
                </div>
                <div class="col-md-6">
                    <p>> FAQ</p>
                    <p>> SOPORTE</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 footer3 center">
            <img src="<?= base_url('img/icono-facebook.png') ?>">
            <img src="<?= base_url('img/icono-twitter.png') ?>">
            <img src="<?= base_url('img/icono-linkedin.png') ?>">
            <img src="<?= base_url('img/icono-google.png') ?>">
        </div>
    </div>
    <div class="footer">CONTALIA 2014. TODOS LOS DERECHOS RESERVADOS</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('js/docs.min.js') ?>"></script>
<script src="<?= base_url('js/efectos.js') ?>"></script>
</body>
</html>