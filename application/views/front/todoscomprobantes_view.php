<?php
$sql = "SELECT nombre_cliente, total FROM comprobante WHERE id_user = ? ORDER BY id_factura DESC LIMIT 5";
$result = $this->db->query($sql, array($this->tank_auth->get_user_id()));
$row = $result->result();
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url('img/favicon.ico') ?>">

    <title>Ingresos de <?= $usuario ?></title>

    <link href="<?= base_url('css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/dashboard.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/login.css') ?>" rel="stylesheet">

    <?php
    foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
    <style>
        tr {
            background-color: transparent;
        }
    </style>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid menu">
        <div class="logo">
             <a href="<?= base_url('dashboard') ?>"><img src="<?= base_url('img/contalia.png') ?>"></a>
        </div>

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav cont-menu">
                <li><a href="<?= base_url('dashboard/tu_perfil') ?>">TU PERFIL</a></li>
                <li><a href="<?= base_url('dashboard/comprobantes/'.date("F")) ?>">MOVIMIENTOS</a></li>
                <li><a href="<?= base_url('dashboard/historial') ?>">HISTORIAL</a></li>
                <li><a href="#">SOPORTE</a></li>
                <li><a href="<?= base_url('auth/logout') ?>">CERRAR SESIÓN</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="cont-ini-gral">
    <div class="info-user">
        <span class="span1">USUARIO: <a class="user" href="<?= base_url('dashboard') ?>"> <?= $usuario ?></a></span>
        <span class="span2">FECHA: <?= $fecha ?></span>
    </div>

    <div class="info-bills"></div>
    <div class="info-bills2">
        <h4 class="center"><b>FACTURAS RECIENTES</b></h4>
        <div class="row">
            <div class="col-md-8">
                <p>EJEMPLO FACTURA NUMERO 1</p>
                <p>EJEMPLO FACTURA NUMERO 2</p>
                <p>EJEMPLO FACTURA NUMERO 3</p>
                <p>EJEMPLO FACTURA NUMERO 4</p>
                <p>EJEMPLO FACTURA NUMERO 5</p>
            </div>
            <div class="col-md-4">
                <p>$5,000</p>
                <p>$10,000</p>
                <p>$12,000</p>
                <p>$124,000</p>
                <p>$1,277,000</p>
            </div>
        </div>
    </div>
</div>

    <div class="title center"> <h3>INGRESOS / GASTOS</h3> </div>

    <div class="cont-load">
        <?php echo $error;?>
        <!--<h4 class="sub-header subtitle"></h4> Quitar clase-->
        <form class="load-file form-load first" action="<?= base_url("dashboard/subir_archivo/"."ingresos") ?>" method="POST" id="upload_file" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-8 wrapper-file-input btn btn-default">
                    <span class="file-input">SELECCIONAR ARCHIVOS <span class="glyphicon glyphicon-search icon"></span></span>
                    <input type="file" name="files[]" multiple required/>
                </div>
                <div class="col-md-4">
                    <?php echo form_submit('subirArchivo', 'SUBIR ARCHIVO'); ?>
                    <?php echo form_close(); ?>
                </div>
            </div> 
        </form>
        <div class="form-load second">
            <div class="row">
                <div class="col-md-8"> 
                    <button type="button" class="btn btn-default btn-month dropdown-toggle" data-toggle="dropdown"> SELECCIONA MES 
                        <!--<span class="" ><img style="width:15%;" src="<?= base_url('img/logo-select.png') ?>"></span>-->    
                        <span class="glyphicon glyphicon-collapse-down icon"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a id="mesJanuary" href="<?= base_url("dashboard/comprobantes/January") ?>" >Enero</a></li> <!--class="btn btn-default btn-month"-->
                        <li><a id="mesFebruary" href="<?= base_url("dashboard/comprobantes/February") ?>" >Febrero</a></li>
                        <li><a id="mesMarch" href="<?= base_url("dashboard/comprobantes/March") ?>" >Marzo</a></li>
                        <li><a id="mesApril" href="<?= base_url("dashboard/comprobantes/April") ?>" >Abril</a></li>
                        <li><a id="mesMay" href="<?= base_url("dashboard/comprobantes/May") ?>" >Mayo</a></li>
                        <li><a id="mesJune" href="<?= base_url("dashboard/comprobantes/June") ?>" >Junio</a></li>
                        <li><a id="mesJuly" href="<?= base_url("dashboard/comprobantes/July") ?>" >Julio</a></li>
                        <li><a id="mesAugust" href="<?= base_url("dashboard/comprobantes/August") ?>" >Agosto</a></li>
                        <li><a id="mesSeptember" href="<?= base_url("dashboard/comprobantes/September") ?>" >Septiembre</a></li> <!--class="btn btn-default btn-month2-->
                        <li><a id="mesOctober" href="<?= base_url("dashboard/comprobantes/October") ?>" >Octubre</a></li><!--class="btn btn-default btn-month"-->
                        <li><a id="mesNovember" href="<?= base_url("dashboard/comprobantes/November") ?>" >Noviembre</a></li><!--class="btn btn-default btn-month2-->
                        <li><a id="mesDecember" href="<?= base_url("dashboard/comprobantes/December") ?>" >Diciembre</a></li>

                        <li class="divider"></li>
                        <li><a href="<?= base_url("dashboard/todosComprobantes") ?>" >Ver Todos</a></li>
                    </ul>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
        <div class="form-load third center">
            <p><span>MES:</span> OCTUBRE</p> <!--Actualizar-->
        </div>

        <div class="container-fluid cont_principal" style="background:#FFF; padding:0; border-bottom: 1.5em solid #707070;">  
            <?php echo $output; ?>  
        </div> 
    </div>
            
            
            <!--<div class="btn-group btn-group-lg">
                <a id="mesJanuary" href="<?= base_url("dashboard/comprobantes/January") ?>" class="btn btn-default btn-month">Enero</a>
                <a id="mesFebruary" href="<?= base_url("dashboard/comprobantes/February") ?>" class="btn btn-default btn-month">Febrero</a>
                <a id="mesMarch" href="<?= base_url("dashboard/comprobantes/March") ?>" class="btn btn-default btn-month">Marzo</a>
                <a id="mesApril" href="<?= base_url("dashboard/comprobantes/April") ?>" class="btn btn-default btn-month">Abril</a>
                <a id="mesMay" href="<?= base_url("dashboard/comprobantes/May") ?>" class="btn btn-default btn-month">Mayo</a>
                <a id="mesJune" href="<?= base_url("dashboard/comprobantes/June") ?>" class="btn btn-default btn-month">Junio</a>
                <a id="mesJuly" href="<?= base_url("dashboard/comprobantes/July") ?>" class="btn btn-default btn-month">Julio</a>
                <a id="mesAugust" href="<?= base_url("dashboard/comprobantes/August") ?>" class="btn btn-default btn-month">Agosto</a>
                <a id="mesSeptember" href="<?= base_url("dashboard/comprobantes/September") ?>" class="btn btn-default btn-month2">Septiembre</a>
                <a id="mesOctober" href="<?= base_url("dashboard/comprobantes/October") ?>" class="btn btn-default btn-month">Octubre</a>
                <a id="mesNovember" href="<?= base_url("dashboard/comprobantes/November") ?>" class="btn btn-default btn-month2">Noviembre</a>
                <a id="mesDecember" href="<?= base_url("dashboard/comprobantes/December") ?>" class="btn btn-default btn-month2">Diciembre</a>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <a href="<?= base_url("dashboard/todosComprobantes") ?>" class="btn btn-default btn-month">Ver Todos</a>
                </div>
            </div>-->
            
            
                 
    <div class="cont-ini-gral-lower center">
        <div class="cont-p-page">
            <div class="p-page1">
                <h4>POR COBRAR</h4>
                <hr>
                <h2>$297,430.00</h2>
            </div>
            <div class="p-page2">
                <h4>POR PAGAR</h4>
                <hr>
                <h2>$182,500.00</h2>  
            </div>
        </div>
    </div>
<!--Footer-->
<div class="row cont-footer">
    <div class="col-md-5 footer1">
        <img src="<?= base_url('img/logo-contalia.png') ?>">
        <p><span class="glyphicon glyphicon-phone-alt"></span> 6305 6496 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="glyphicon glyphicon-envelope"></span> contacto@contalia.com.mx
        </p>
    </div>
    <div class="col-md-3 footer2">
        <h4>SISTEMA</h4>
        <div class="row">
            <div class="col-md-6">
                <p>> COMO FUNCIONA</p>
                <p>> PAQUETES</p>
            </div>
            <div class="col-md-6">
                <p>> FAQ</p>
                <p>> SOPORTE</p>
            </div>
        </div>
    </div>
    <div class="col-md-4 footer3 center">
        <img src="<?= base_url('img/icono-facebook.png') ?>">
        <img src="<?= base_url('img/icono-twitter.png') ?>">
        <img src="<?= base_url('img/icono-linkedin.png') ?>">
        <img src="<?= base_url('img/icono-google.png') ?>">
    </div>
</div>
<div class="footer">CONTALIA 2014. TODOS LOS DERECHOS RESERVADOS</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="--><?//= base_url('js/jquery.min.js') ?><!--"></script>-->
<script>
    var mes = "#mes"+"<?php echo $mesactivo ?>";
    $( mes ).toggleClass( "active" );
</script>

<script>
    $('.gasto').closest('tr').css('background-color','#FFC3C0');
    $('.ingreso').closest('tr').css('background-color','#CCFFCC');
</script>

<script src="<?= base_url('js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('js/docs.min.js') ?>"></script>
<script src="<?= base_url('js/efectos.js') ?>"></script>
</body>
</html>