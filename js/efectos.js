$(document).ready(efectos());

function efectos() {
    $(".alert-success").delay(3000).fadeOut();

    $( ".item-perfil" ).click(function(e) {
        e.preventDefault();
        $(this).siblings().removeClass('active');
        $( this ).toggleClass( "active" );
    });

    $("#listoPerfil").click(function(){
        var htmlString = "Da aviso a tu contador que estas listo para enviarle el reporte";
        $( "#contenidoPerfil" ).text( htmlString );
    });

    $("#planPerfil").click(function(){
        var htmlString = "Verifica tu plan";
        $( "#contenidoPerfil" ).text( htmlString );
    });

    $("#contadorPerfil").click(function(){
        $( "#contenidoPerfil" ).text( "" );
        /*$.getJSON( "http://sistema.contalia.com.mx/dashboard/revisarContador", function( data ) {
            console.log(data);
            $.each( data, function( key, val ) {
                if(val == 1) {
                    alert("Ya tiene contador");
                }
                else {
                    alert("Aun no tiene contador");
                }
            });
        });*/
        $.getJSON( "http://sistema.contalia.com.mx/dashboard/listaContadores", function( data ) {
            var items = [];
            $.each( data, function( key, val ) {
                items.push( "<li><a href='http://sistema.contalia.com.mx/dashboard/infoContador/"+ key +"' target='_self'>" + val + "</a></li>" );
            });

            $( "<ul/>", {
                "class": "lista-contadores",
                html: items.join( "" )
            }).appendTo( "#contenidoPerfil" );
        });
    });

    $("#soportePerfil").click(function(){
        var htmlString = "Mandanos un correo";
        $( "#contenidoPerfil" ).text( htmlString );
    });
}
